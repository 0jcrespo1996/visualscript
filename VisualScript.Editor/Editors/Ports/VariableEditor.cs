﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using JetBrains.Annotations;
using VisualScript.Core.Runtime.Ports;
using VisualScript.CoreNodes;
using VisualScript.CoreNodes.Variables;
using VisualScript.Editor.Views.Port;

namespace VisualScript.Editor.Editors.Ports
{
    [EnumeratorFor(typeof(GetVariableNode), nameof(GetVariableNode.VarName))]
    [EnumeratorFor(typeof(SetVariableNode), nameof(SetVariableNode.VarName))]
    public class VariableEditor : PortEditor
    {
        static VariableEditor()
        {
            Register<string, VariableEditor>();
        }
        
        public ComboBox ComboBox { get; }
        
        public VariableEditor([NotNull] PortView view, [NotNull] IValuePort valuePort, [NotNull] StackPanel stackPanel) : base(view, valuePort, stackPanel)
        {
            ComboBox = new ComboBox() {Visibility = Visibility.Collapsed};
            ComboBox.MouseLeftButtonDown += (_, e) => e.Handled = true;
            View.PortGrid.Children.Add(ComboBox);
            Grid.SetRow(ComboBox, 1);
            Grid.SetColumn(ComboBox, 2);
            var binding = new Binding() {Source = View.Node.Manager.Globals};
            ComboBox.SetBinding(ItemsControl.ItemsSourceProperty, binding);

            
            
            var template = new DataTemplate();

            var factory = new FrameworkElementFactory(typeof(TextBlock));

            var textBinding = new Binding("Key");
            factory.SetBinding(TextBlock.TextProperty, textBinding);

            template.VisualTree = factory;
            
            ComboBox.ItemTemplate = template;
        }
        
        public override void Enable()
        {
            base.Enable();
            ComboBox.Visibility = Visibility.Visible;
            ComboBox.SelectionChanged += ComboBoxOnSelectionChanged;
        }

        private void ComboBoxOnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selected = (KeyValuePair<string, object?>) e.AddedItems[0]!;
            ValuePort.ConstantValue = selected.Key;
        }

        public override void Disable()
        {
            base.Disable();
            ComboBox.Visibility = Visibility.Collapsed;
            ComboBox.SelectionChanged -= ComboBoxOnSelectionChanged;
        }
    }
}