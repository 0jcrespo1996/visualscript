﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using JetBrains.Annotations;
using VisualScript.Core.Runtime.Ports;
using VisualScript.Editor.Utils;
using VisualScript.Editor.Views.Port;

namespace VisualScript.Editor.Editors.Ports
{
    public class EnumEditor : PortEditor
    {

        static EnumEditor()
        {
            Register<Enum, EnumEditor>();
        }
        
        public ComboBox ComboBox { get; }
        public EnumEditor([NotNull] PortView view, [NotNull] IValuePort valuePort, [NotNull] StackPanel stackPanel) : base(view, valuePort, stackPanel)
        {
            ComboBox = new ComboBox();
            if (ValuePort.ConstantValue != null)
                ComboBox.SelectedItem = ValuePort.ConstantValue;

            if (!ValuePort.IsInput)
                ComboBox.FlowDirection = FlowDirection.LeftToRight;
            View.PortGrid.Children.Add(ComboBox);
            Grid.SetRow(ComboBox, 1);
            Grid.SetColumn(ComboBox, 2);
            ComboBox.ItemTemplate = new DataTemplate();
            var factory = new FrameworkElementFactory(typeof(TextBlock));
            var binding = new Binding {Converter = new EnumConverter()};
            factory.SetBinding(TextBlock.TextProperty, binding);
            var values = Enum.GetValues(ValuePort.ValueType);
            ComboBox.ItemTemplate.VisualTree = factory;
            foreach (var value in values)
                ComboBox.Items.Add(value);
        }


        public override void Enable()
        {
            base.Enable();
            ComboBox.SelectionChanged += ComboBoxOnSelectionChanged;
            ComboBox.Visibility = Visibility.Visible;
        }

        public override void Disable()
        {
            base.Disable();
            ComboBox.SelectionChanged -= ComboBoxOnSelectionChanged;
            ComboBox.Visibility = Visibility.Collapsed;
        }

        private void ComboBoxOnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0) return;
            ValuePort.ConstantValue = e.AddedItems[0];
        }
    }
}