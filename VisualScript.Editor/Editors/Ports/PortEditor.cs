﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.Editor.Views.Port;

namespace VisualScript.Editor.Editors.Ports
{
    public abstract class PortEditor
    {
        static PortEditor()
        {
            var types = Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => !t.IsAbstract && t.IsSubclassOf(typeof(PortEditor)));
            foreach (var type in types)
                RuntimeHelpers.RunClassConstructor(type.TypeHandle);
        }
        
        public PortView View { get; }
        public IValuePort ValuePort { get; }
        public StackPanel StackPanel { get; }
        
        public bool IsEnabled { get; private set; }
        
        public bool Enumerate { get; }

        protected PortEditor(PortView view, IValuePort valuePort, StackPanel stackPanel)
        {
            View = view;
            ValuePort = valuePort;
            StackPanel = stackPanel;
            Enumerate = valuePort.GetType().IsDefined(typeof(EnumerateAttribute));
        }

        public virtual void Enable()
        {
            if (IsEnabled) return;
            IsEnabled = true;
        }

        public virtual void Disable()
        {
            if (!IsEnabled) return;
            IsEnabled = false;
        }


        private static readonly Dictionary<Type, Type> _registered = new();

        private static readonly Dictionary<(Type, Type, string), Type> _registeredEnumerators = new();

        public static void Register<TValue, TEditor>() where TEditor : PortEditor =>
            Register(typeof(TValue), typeof(TEditor));

        public static void Register(Type valueType, Type editorType)
        {
            if (editorType.IsDefined(typeof(EnumeratorFor)))
            {
                foreach (var forAttr in editorType.GetCustomAttributes<EnumeratorFor>())
                    _registeredEnumerators.Add((valueType, forAttr.Type, forAttr.Property), editorType);
            }
            else _registered.Add(valueType, editorType);
        }

        public static bool TryGetEditor(Type valueType, out Type editorType)
        {
            if (valueType.IsSubclassOf(typeof(Enum)))
                valueType = typeof(Enum);
            return _registered.TryGetValue(valueType, out editorType!);
        }

        public static bool TryGetEnumerator(Type valueType, Type nodeType, string property, out Type editorType)
        {
            return _registeredEnumerators.TryGetValue((valueType, nodeType, property), out editorType!);
        }
    }
}