﻿using System;
using VisualScript.Core.Attributes;

namespace VisualScript.Editor.Editors.Ports
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class EnumeratorFor : VisualAttribute
    {
        public Type Type { get; }
        public string Property { get; }

        public EnumeratorFor(Type nodeType, string property)
        {
            Type = nodeType;
            Property = property;
        }
    }
}