﻿using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using JetBrains.Annotations;
using VisualScript.Core;
using VisualScript.Core.Runtime.Ports;
using VisualScript.Editor.Views.Port;
using Xceed.Wpf.Toolkit;

namespace VisualScript.Editor.Editors.Ports
{
    public class NumberEditor : PortEditor
    {
        
        static NumberEditor()
        {
            Register<Number, NumberEditor>();
            Register<sbyte, NumberEditor>();
            Register<byte, NumberEditor>();
            Register<short, NumberEditor>();
            Register<ushort, NumberEditor>();
            Register<int, NumberEditor>();
            Register<uint, NumberEditor>();
            Register<long, NumberEditor>();
            Register<ulong, NumberEditor>();
            Register<float, NumberEditor>();
            Register<double, NumberEditor>();
        }
        
        public TextBox TextBox { get; }
        
        public NumberEditor([NotNull] PortView view, [NotNull] IValuePort valuePort, [NotNull] StackPanel stackPanel) : base(view, valuePort, stackPanel)
        {
            TextBox = new TextBox()
                {Text = valuePort.ConstantValue is Number num ? num.ToString(CultureInfo.InvariantCulture) : "0"};
            
            View.PortGrid.Children.Add(TextBox);
            Grid.SetRow(TextBox, 1);
            Grid.SetColumn(TextBox, 2);
            if (!ValuePort.IsInput)
                TextBox.FlowDirection = FlowDirection.LeftToRight;
        }

        public override void Enable()
        {
            base.Enable();
            TextBox.Visibility = Visibility.Visible;
            TextBox.PreviewTextInput += TextBoxOnPreviewTextInput;
            TextBox.TextChanged += TextBoxOnTextChanged;
        }

        public override void Disable()
        {
            base.Disable();
            TextBox.Visibility = Visibility.Collapsed;
            TextBox.TextChanged -= TextBoxOnTextChanged;
        }

        private void TextBoxOnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (Number.TryParse(TextBox.Text, out var number))
                ValuePort.ConstantValue = number;
        }

        private static void TextBoxOnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !Regex.IsMatch(e.Text, @"^\-?[0-9]*\.?[0-9]*$");
        }
    }
}