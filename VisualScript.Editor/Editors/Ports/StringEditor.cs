﻿using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using JetBrains.Annotations;
using VisualScript.Core;
using VisualScript.Core.Runtime.Ports;
using VisualScript.Editor.Utils;
using VisualScript.Editor.Views.Port;
using Xceed.Wpf.Toolkit;

namespace VisualScript.Editor.Editors.Ports
{
    public class StringEditor : PortEditor
    {
        
        static StringEditor()
        {
            Register<string, StringEditor>();
        }
        
        public TextBox TextBox { get; }
        
        public StringEditor([NotNull] PortView view, [NotNull] IValuePort valuePort, [NotNull] StackPanel stackPanel) : base(view, valuePort, stackPanel)
        {
            TextBox = new TextBox()
                {Text = valuePort.ConstantValue?.ToString()};
            
            View.PortGrid.Children.Add(TextBox);
            Grid.SetRow(TextBox, 1);
            Grid.SetColumn(TextBox, 2);
            if (!ValuePort.IsInput)
                TextBox.FlowDirection = FlowDirection.LeftToRight;
        }

        public override void Enable()
        {
            base.Enable();
            TextBox.Visibility = Visibility.Visible;
            TextBox.TextChanged += TextBoxOnTextChanged;
        }

        public override void Disable()
        {
            base.Disable();
            TextBox.Visibility = Visibility.Collapsed;
            TextBox.TextChanged -= TextBoxOnTextChanged;
        }

        private void TextBoxOnTextChanged(object sender, TextChangedEventArgs e)
        {
            ValuePort.ConstantValue = TextBox.Text;
        }
        
    }
}