﻿using System;
using System.IO;
using JetBrains.Annotations;
using Microsoft.Win32;

namespace VisualScript.Editor.Windows.Main
{
    public class SaveFileCommand : MainWindowCommand
    {
        public SaveFileCommand([NotNull] MainWindow window) : base(window)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return true;
        }

        public override void Execute(object? parameter)
        {
            var dialog = new SaveFileDialog
            {
                InitialDirectory = Environment.CurrentDirectory,
                AddExtension = true,
                DefaultExt = ".vsi",
                Filter = "VisualScript instructions file (*.vsi)|*.vsi"
            };
            
            if (dialog.ShowDialog(Window) == true)
            {
                var ser = Window.Manager.Manager.Serialize();
                File.WriteAllBytes(dialog.FileName, ser);
            }
        }
    }
}