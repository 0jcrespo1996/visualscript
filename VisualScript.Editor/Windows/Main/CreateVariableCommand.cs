﻿using JetBrains.Annotations;
using VisualScript.Editor.Dialogs;
using VisualScript.Editor.Utils;

namespace VisualScript.Editor.Windows.Main
{
    public class CreateVariableCommand : MainWindowCommand
    {
        public CreateVariableCommand([NotNull] MainWindow window) : base(window)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return true;
        }

        public override void Execute(object? parameter)
        {
            Window.Dispatcher.Invoke(() =>
            {
                var name = InputDialog.Show("Insert the variable name");
                if (string.IsNullOrWhiteSpace(name)) return;
                if (Window.Manager.Globals.ContainsKey(name)) return;
                Window.Manager.Manager.SetVariable(name, null);
            });
        }
    }
}