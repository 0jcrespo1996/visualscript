﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using JetBrains.Annotations;
using VisualScript.Editor.Utils;

namespace VisualScript.Editor.Windows.Main
{
    public class DragStartCommand : MainWindowCommand
    {
        public DragStartCommand([NotNull] MainWindow window) : base(window)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return !Window.Manager.IsSelectingNodes && Window.TreeView.SelectedItem is TreeViewItem treeItem &&
                   treeItem.Header is NodeMeta && Mouse.LeftButton == MouseButtonState.Pressed;
        }


        public override void Execute(object? parameter)
        {
            Window.IsDragging = true;
            DragDrop.DoDragDrop(Window.TreeView, new DataObject(typeof(NodeMeta), Window.TreeView.SelectedItem),
                DragDropEffects.All);
            Window.IsDragging = false;
        }
    }
}