﻿using JetBrains.Annotations;

namespace VisualScript.Editor.Windows.Main
{
    public class DeleteNodesCommand : MainWindowCommand
    {
        public DeleteNodesCommand([NotNull] MainWindow window) : base(window)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return Window.Manager.IsMouseInside;
        }

        public override void Execute(object? parameter)
        {
            Window.Manager.DeleteSelected();
        }
    }
}