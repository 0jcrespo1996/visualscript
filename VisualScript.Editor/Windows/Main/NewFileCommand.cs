﻿using JetBrains.Annotations;

namespace VisualScript.Editor.Windows.Main
{
    public class NewFileCommand : MainWindowCommand

    {
        public NewFileCommand([NotNull] MainWindow window) : base(window)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return true;
        }

        public override void Execute(object? parameter)
        {
            Window.Manager.Clear();
        }
    }
}