﻿using System;
using System.Windows.Input;

namespace VisualScript.Editor.Windows.Main
{
    public abstract class MainWindowCommand : ICommand
    {
        public MainWindow Window { get; }

        protected MainWindowCommand(MainWindow window)
        {
            Window = window;
        }
        
        public abstract bool CanExecute(object? parameter);

        public virtual void Execute(object? parameter)
        {
            
        }

        protected void OnExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

#pragma warning disable CS0067
        public event EventHandler? CanExecuteChanged;
#pragma warning restore CS0067
    }
}