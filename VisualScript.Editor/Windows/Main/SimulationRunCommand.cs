﻿using JetBrains.Annotations;
using VisualScript.Core;

namespace VisualScript.Editor.Windows.Main
{
    public class SimulationRunCommand : MainWindowCommand
    {
        public bool IsRunning { get; private set; }
        
        public SimulationRunCommand([NotNull] MainWindow window) : base(window)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return !IsRunning;
        }

        public override async void Execute(object? parameter)
        {
            IsRunning = true;
            OnExecuteChanged();
            var entry = Window.Manager.Manager.GetEntryNode();
            if (entry == null)
            {
                IsRunning = false;
                OnExecuteChanged();
                return;
            }

            await entry.RunAsync(new Context());
            IsRunning = false;
            OnExecuteChanged();
        }
    }
}