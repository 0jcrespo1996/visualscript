﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Serilog;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Editor.Dialogs;
using VisualScript.Editor.Resources;
using VisualScript.Editor.Utils;

namespace VisualScript.Editor.Windows.Main
{
    
    

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public TreeViewItem NodesItem { get; }
        
        public MenuItem NodesMenuItem { get; }
        
        
        public bool IsDragging { get; set; }
        
        public DragStartCommand DragStartCommand { get; }
        
        public SimulationRunCommand SimulationRunCommand { get; }
        
        public DeleteNodesCommand DeleteNodesCommand { get; }
        
        
        public NewFileCommand NewFileCommand { get; }
        
        public SaveFileCommand SaveFileCommand { get; }
        public LoadFileCommand LoadFileCommand { get; }
        
        public CreateVariableCommand CreateVariableCommand { get; }
        
        public MainWindow()
        {
            DragStartCommand = new DragStartCommand(this);
            SimulationRunCommand = new SimulationRunCommand(this);
            DeleteNodesCommand = new DeleteNodesCommand(this);
            NewFileCommand = new NewFileCommand(this);
            SaveFileCommand = new SaveFileCommand(this);
            LoadFileCommand = new LoadFileCommand(this);
            CreateVariableCommand = new CreateVariableCommand(this);
            InitializeComponent();
            NodesItem = new TreeViewItem() {Header = EditorStrings.Nodes};
            NodesMenuItem = new MenuItem() {Header = EditorStrings.Nodes};
            TreeView.Items.Insert(0, NodesItem);
            Manager.Manager.ExecutionDelay = 150;
            _ = new DirectoryCatalog(".", "*.dll");
            Manager.ContextMenu = new ContextMenu();
            Manager.ContextMenu.Items.Add(NodesMenuItem);
            Manager.ContextMenu.Opened += ContextMenuOnOpened;
            UpdateNodesList();
        }
        

        private void ContextMenuOnOpened(object sender, RoutedEventArgs e)
        {
            NodesMenuItem.Items.Clear();
            var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetExportedTypes())
                .Where(t => t.IsAssignableTo(typeof(INode)) && !t.IsInterface && !t.IsAbstract);
            
            foreach (var type in types)
            {
                var meta = new NodeMeta(type);
                var stack = new Stack<string>(meta.Category.Reverse());

                var current = NodesMenuItem;

                while (stack.TryPop(out var part))
                {
                    var next = current.Items.Cast<MenuItem>()
                        .FirstOrDefault(i => i != null && i.Header.ToString() == part);

                    if (next == null)
                    {
                        next = new MenuItem() {Header = part};
                        current.Items.Add(next);
                    }

                    current = next;
                }

                var item = new MenuItem() {Header = meta, ToolTip = meta.Description};
                item.Click += ItemOnClick;
                current.Items.Add(item);
            }
        }

        private void ItemOnClick(object sender, RoutedEventArgs e)
        {
            if (!((sender as MenuItem)?.Header is NodeMeta data)) return;
            var type = data.Type;
            try
            {
                Manager.CreateNode(type, Manager.MousePosition);
            }
            catch (Exception exception)
            {
                if (exception.InnerException is OperationCanceledException)
                    return;
                Log.Error("{Message}",exception.Message);
            }
        }


        public void UpdateNodesList()
        {
            NodesItem.Items.Clear();
            var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetExportedTypes())
                .Where(t => t.IsAssignableTo(typeof(INode)) && !t.IsInterface && !t.IsAbstract);
            
            foreach (var type in types)
            {
                var meta = new NodeMeta(type);
                var stack = new Stack<string>(meta.Category.Reverse());

                var current = NodesItem;

                while (stack.TryPop(out var part))
                {
                    var next = current.Items.Cast<TreeViewItem>()
                        .FirstOrDefault(i => i != null && i.Header.ToString() == part);

                    if (next == null)
                    {
                        next = new TreeViewItem() {Header = part};
                        current.Items.Add(next);
                    }

                    current = next;
                }

                var item = new TreeViewItem {Header = meta, ToolTip = meta.Description};
                current.Items.Add(item);
            }
        }
    }
}