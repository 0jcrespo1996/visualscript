﻿using System;
using System.IO;
using JetBrains.Annotations;
using Microsoft.Win32;

namespace VisualScript.Editor.Windows.Main
{
    public class LoadFileCommand : MainWindowCommand
    {
        public LoadFileCommand([NotNull] MainWindow window) : base(window)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return true;
        }

        public override void Execute(object? parameter)
        {
            var dialog = new OpenFileDialog
            {
                InitialDirectory = Environment.CurrentDirectory,
                AddExtension = true,
                DefaultExt = ".vsi",
                Filter = "VisualScript instructions file (*.vsi)|*.vsi"
            };
            
            if (dialog.ShowDialog(Window) == true)
            {
                var bytes = File.ReadAllBytes(dialog.FileName);
                Window.Manager.Clear();
                Window.Manager.Deserialize(bytes);
            }
        }
    }
}