﻿using System;
using VisualScript.Core.Attributes;

namespace VisualScript.Editor.Wizards
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class WizardFor : VisualAttribute
    {
        public Type NodeType { get; }

        public WizardFor(Type nodeType)
        {
            NodeType = nodeType;
        }
    }
}