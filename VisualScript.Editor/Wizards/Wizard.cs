﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using VisualScript.Core;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Editor.Views.Manager;

namespace VisualScript.Editor.Wizards
{
    public abstract class Wizard
    {
        static Wizard()
        {
            var types = Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => !t.IsAbstract && t.IsSubclassOf(typeof(Wizard)) && t.IsDefined(typeof(WizardFor)));
            foreach (var type in types)
            {
                RuntimeHelpers.RunClassConstructor(type.TypeHandle);
                var forType = type.GetCustomAttribute<WizardFor>()!.NodeType;
                _registered[forType] = (Wizard) Activator.CreateInstance(type)!;
            }
        }

        public abstract bool TrySetup(NodesManagerView manager, out INode node, out Exception? exception);

        private static readonly Dictionary<Type, Wizard> _registered = new();

        public static bool TryGetWizard(Type type, out Wizard wizard)
        {
            if (type.IsSubclassOf(typeof(Enum)))
                type = typeof(Enum);
            return _registered.TryGetValue(type, out wizard!);
        }
    }
}