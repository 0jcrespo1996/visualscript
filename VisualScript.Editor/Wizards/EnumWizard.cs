﻿using System;
using System.Collections.Generic;
using System.Linq;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.CoreNodes.Constants;
using VisualScript.CoreNodes.Utils;
using VisualScript.Editor.Dialogs;
using VisualScript.Editor.Editors.Ports;
using VisualScript.Editor.Views.Manager;

namespace VisualScript.Editor.Wizards
{
    [WizardFor(typeof(EnumNode<>))]
    public class EnumWizard : CastNodeWizard
    {
        public override bool TrySetup(NodesManagerView manager, out INode node, out Exception exception)
        {
            node = null!;
            exception = null!;
            var types = GetTypes();
            var selected = ListSelectDialog.ShowDialog("Select the target type", types) as Type;
            if (selected == null)
            {
                exception = new OperationCanceledException();
                return false;
            }

            var newType = typeof(EnumNode<>).MakeGenericType(selected);
            node = manager.Manager.CreateNode(newType, Guid.NewGuid());
            return true;
        }


        public override IEnumerable<Type> GetTypes()
        {
            return base.GetTypes().Where(t => t.IsSubclassOf(typeof(Enum)));
        }
    }
}