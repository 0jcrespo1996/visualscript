﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace VisualScript.Editor.Dialogs
{
    public partial class ListSelectDialog
    {
        public ObservableCollection<object> Elements { get; }
        
        public bool Success { get; private set; }
        
        public RelayCommand OkCommand { get; }
        
        public ICommand SearchCommand { get; }
        
        public long ElementsCount { get; }

        private readonly object[] _values;
        
        public object? Selected { get; private set; }
        public ListSelectDialog(object prompt, IEnumerable<object> values)
        {
            _values = values.ToArray();
            ElementsCount = _values.LongLength;
            Elements = new ObservableCollection<object>(_values.Take(40));
            OkCommand = new RelayCommand(OnOkClick, () => ListView.SelectedItem != null);
            SearchCommand = new RelayCommand(OnSearch, () => SearchBox.IsKeyboardFocused);
            InitializeComponent();
            Prompt.Content = prompt;
        }

        private void OnSearch()
        {
            Elements.Clear();
            foreach (var value in _values)
                if (value.ToString()?.Contains(SearchBox.Text, StringComparison.OrdinalIgnoreCase) ?? false)
                    Elements.Add(value);
        }
        private void OnOkClick()
        {
            Success = true;
            Selected = ListView.SelectedItem;
            Close();
        }

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public static object? ShowDialog(object prompt, IEnumerable<object> elements)
        {
            var window = new ListSelectDialog(prompt, elements);
            window.ShowDialog();
            return window.Selected;
        }

        private void ListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OkCommand.RaiseCanExecuteChanged();
        }

        private void ListView_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OnOkClick();
        }
    }
}