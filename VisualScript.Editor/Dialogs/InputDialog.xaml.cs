﻿using System.Windows;

namespace VisualScript.Editor.Dialogs
{
    public partial class InputDialog : Window
    {
        public bool Success { get; private set; }
        public InputDialog()
        {
            InitializeComponent();
        }

        public static string? Show(string? prompt = null)
        {
            prompt ??= string.Empty;
            var dialog = new InputDialog {Prompt = {Content = prompt}};
            dialog.ShowDialog();
            return dialog.Success ? dialog.Input.Text : null;
        }

        private void OkClicked(object sender, RoutedEventArgs e)
        {
            Success = true;
            Close();
        }

        private void CancelClicked(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}