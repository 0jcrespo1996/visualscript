﻿using System.IO;
using System.Text;
using System.Windows.Controls;

namespace VisualScript.Editor.Utils
{
    public class TextBoxWriter : TextWriter
    {
        public TextBox TextBox { get; }

        public override Encoding Encoding { get; } = Encoding.UTF8;
        
        public TextBoxWriter(TextBox textBox)
        {
            TextBox = textBox;
        }

        public override void Write(char value)
        {
            TextBox.Text = value + TextBox.Text;
        }

        public override void Write(string? value)
        {
            TextBox.Text = value + TextBox.Text;
        }
    }
}