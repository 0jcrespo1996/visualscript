﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media;

namespace VisualScript.Editor.Utils
{
    public class BrushToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is not Brush brush)
                throw new InvalidDataException();

            if (brush is SolidColorBrush solidColorBrush)
                return solidColorBrush.Color;

            throw new InvalidDataException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var color = (Color) value;
            return new SolidColorBrush(color);
        }
    }
}