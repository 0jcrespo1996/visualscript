﻿using System;
using System.Reflection;
using VisualScript.Core.Attributes;

namespace VisualScript.Editor.Utils
{
    public readonly struct NodeMeta
    {
        public string Name { get; }
        public Type Type { get; }
        
        public string? Description { get; }
        public string[] Category { get; }

        public NodeMeta(Type type)
        {
            Name = type.GetCustomAttribute<NodeAttribute>()?.Name ?? type.Name;
            Type = type;
            Description = type.GetCustomAttribute<DescriptionAttribute>()?.Description;
            Category = type.GetCustomAttribute<CategoryAttribute>()?.Path ?? new[] {CategoryAttribute.DefaultPath};
        }
        
        public override string ToString()
        {
            return Name;
        }
    }
}