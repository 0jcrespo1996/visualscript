﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using VisualScript.Editor.Views.Port;

namespace VisualScript.Editor.Utils
{
    public static class Extensions
    {
        public static void Empty<T>(this ObservableCollection<T> collection, Action<T>? callback = null)
        {
            while (collection.Count > 0)
                callback?.Invoke(collection[0]);
        }

        public static Vector GetCenter(this PortView port)
        {
            var point = port.Shape.TranslatePoint(new Point(0, 0), port.Node.Manager.Canvas);

            if (port.Port.IsInput)
                point = new Point(point.X - port.Shape.ActualWidth, point.Y);

            return new Vector(point.X + (port.Shape.ActualWidth / 2), point.Y + (port.Shape.ActualHeight / 2));
        }
    }
}