﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Serilog;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.Editor.Utils;
using VisualScript.Editor.Views.Port;

namespace VisualScript.Editor.Views
{
    public partial class ConnectionView : UserControl
    {
        
        
        
        public PortView From { get; private set; }
        public PortView? To { get; set; }
        
        public ConnectionView(PortView from)
        {
            InitializeComponent();
            From = from;
            from.Node.Manager.Canvas.Children.Add(this);
            Loaded += (_, _) => Update();
            From.Shape.Fill = From.Shape.Stroke;
            Panel.SetZIndex(this, -1);
        }

        public void Discard()
        {
            From.Node.Manager.Canvas.Children.Remove(this);
            if (From.Node.Manager.ActiveConnection == this)
                From.Node.Manager.ActiveConnection = null;
            
            From.Shape.Fill = Brushes.Transparent;
            if (To != null)
                To.Shape.Fill = Brushes.Transparent;
        }

        public void Update()
        {
            var centerStart = From.GetCenter();

            var centerTo = To?.GetCenter() ?? centerStart;
            Update(centerStart, centerTo);
        }

        public void Update(Vector end)
        {
            Update(From.GetCenter(), end);
        }
        
        public void Update(Vector centerStart, Vector centerTo)
        {
            var start = centerStart;
            var end = centerTo;
            
            var center = new Point((start.X + end.X) * 0.5, (start.Y + end.Y) * 0.5);

            if (start.X > end.X)
            {
                var temp = start;
                start = end;
                end = temp;
            }
            
            var ratio = Math.Min(1, (center.X - start.X) / 100);
            var c0 = start;
            var c1 = end;
            c0.X += 100 * ratio;
            c1.X -= 100 * ratio;
            
            var data = string.Format( "M{0},{1} C{0},{1} {2},{3} {4},{5} " +
                                      "M{4},{5} C{4},{5} {6},{7} {8},{9}",
                ( int )start.X, ( int )start.Y, // 0, 1
                ( int )c0.X, ( int )c0.Y, // 2, 3
                ( int )center.X, ( int )center.Y, // 4, 5
                ( int )c1.X, ( int )c1.Y, // 6, 7
                ( int )end.X, ( int )end.Y ); // 8.9

            Line.Data = Geometry.Parse(data);
        }

        public void ConnectTo(PortView port)
        {
            if (From.Port.IsInput && !port.Port.IsInput)
            {
                var temp = From;
                From = port;
                port = temp;
            }

            if (!From.Port.CanConnectTo(port.Port))
            {
                Disconnect();
                return;
            }
            
            From.Port.ConnectTo(port.Port);
            To = port;
            From.Node.Manager.ActiveConnection = null;
            From.Connection = this;
            To.Connection = this;
            To.Shape.Fill = To.Shape.Stroke;
            From.Shape.Fill = From.Shape.Stroke;
            Line.Stroke = Brushes.White;
            if (To.Node.Node.InputPorts.OfType<IValuePort>().Any())
                To.Editor?.Disable();
            
            To.Node.Node.Executing += NodeOnExecuting;
            if (To.Port is IValuePort valuePort)
            {
                valuePort.GettingValue += ValuePortOnGettingValue;
                valuePort.GotValue += ValuePortOnGotValue;
            }
            To.Node.Node.Executed += NodeOnExecuted;
            To.Node.Node.Errored += NodeOnErrored;
            Update();
        }

        private void NodeOnErrored(NodeEventArgs arg1, ExecutePort port, Exception exception)
        {
            if (From.Port != port && To?.Port != port) return;
            Log.Error("Error: {Message}", exception.Message);
            Line.Stroke = Brushes.Red;
        }

        private void ValuePortOnGotValue(IValuePort port)
        {
            if (From.Port != port && To?.Port != port) return;
            Line.Stroke = Brushes.White;
        }

        private void ValuePortOnGettingValue(IValuePort port)
        {
            if (From.Port != port && To?.Port != port) return;
            Line.Stroke = Brushes.Brown;
        }

        private void NodeOnExecuted(NodeEventArgs arg1, ExecutePort port)
        {
            if (From.Port != port && To?.Port != port) return;
            Line.Stroke = Brushes.White;
        }

        private void NodeOnExecuting(NodeEventArgs arg1, ExecutePort port)
        {
            if (From.Port != port && To?.Port != port) return;
            Line.Stroke = Brushes.Orange;
        }

        public void Disconnect()
        {
            From.Port.Disconnect();
            From.Connection = null;
            From.Editor?.Enable();
            if (To != null)
            {
                To.Node.Node.Executing -= NodeOnExecuting;
                To.Node.Node.Executed -= NodeOnExecuted;
                To.Node.Node.Errored -= NodeOnErrored;
                if (To.Port is IValuePort valuePort)
                {
                    valuePort.GettingValue -= ValuePortOnGettingValue;
                    valuePort.GotValue -= ValuePortOnGotValue;
                }
                To.Editor?.Enable();
                To.Connection = null;
            }
            Discard();
        }
    }
}