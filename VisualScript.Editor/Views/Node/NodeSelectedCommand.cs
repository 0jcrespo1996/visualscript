﻿using System;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using VisualScript.Editor.Utils;

namespace VisualScript.Editor.Views.Node
{
    public class NodeSelectedCommand : ICommand
    {
        
        public NodeView View { get; }

        public NodeSelectedCommand(NodeView view)
        {
            View = view;
        }
        
        public bool CanExecute(object? parameter)
        {
            return Keyboard.IsKeyUp(Key.LeftShift);
        }

        public void Execute(object? parameter)
        {
            View.Manager.SelectedNodes.Empty(n => n.Deselect());
            View.Select();
            View.Border.Focus();
        }

        #pragma warning disable CS0067
        public event EventHandler? CanExecuteChanged;
        #pragma warning restore CS0067
    }
}