﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using JetBrains.Annotations;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.Editor.Views.Manager;
using VisualScript.Editor.Views.Port;
using Vector = System.Windows.Vector;

namespace VisualScript.Editor.Views.Node
{
    public partial class NodeView
    {
        
        private readonly TranslateTransform _translate = new();
        private readonly ScaleTransform _scaleTransform = new(1, 1);
        
        public NodeSelectedCommand SelectedCommand { get; }

        public bool IsSelected { get; private set; }

        public INode Node { get; } = null!;

        public event Action<Vector>? Moved; 

        public IReadOnlyDictionary<Guid, PortView> InputPorts => _inputPorts;
        private readonly Dictionary<Guid, PortView> _inputPorts = new();

        public IReadOnlyDictionary<Guid, PortView> OutputPorts => _outputPorts;
        private readonly Dictionary<Guid, PortView> _outputPorts = new();

        public NodeView(INode node, NodesManagerView manager) : this()
        {
            Manager = manager;
            Node = node;
            Header = node.Name;
            foreach (var port in node.InputPorts.Concat(node.OutputPorts))
                AddPort(port);
            ToolTip = Node.GetType().GetCustomAttribute<DescriptionAttribute>()?.Description;
            Position = new Vector(Node.Position.X, Node.Position.Y);
        }

        public NodeView()
        {
            SelectedCommand = new NodeSelectedCommand(this);
            InitializeComponent();
            var group = new TransformGroup();
            group.Children.Add(_translate);
            group.Children.Add(_scaleTransform);
            RenderTransform = group;
            
        }
        
        public void Remove()
        {
            foreach (var port in InputPorts.Values.Concat(OutputPorts.Values))
            {
                port.Connection?.Disconnect();
                port.Editor?.Disable();
            }
        }


        public void Select()
        {
            if (IsSelected) return;
            IsSelected = true;
            BorderShadow.Color = Colors.White;
            Manager.SelectedNodes.Add(this);
        }

        public void Deselect()
        {
            
            if (!IsSelected) return;
            IsSelected = false;
            Manager.SelectedNodes.Remove(this);
            BorderShadow.Color = Colors.Black;
        }


        public PortView AddPort(IPort port)
        {
            var dic = port.IsInput ? _inputPorts : _outputPorts;
            var stack = port.IsInput ? LeftStack : RightStack;
            if (dic.TryGetValue(port.Id, out var portView)) return portView;
            portView = new PortView(this, port);
            dic.Add(port.Id, portView);
            if (port is ExecutePort)
            {
                var nonexec = stack.Children.OfType<PortView>().FirstOrDefault(p => p.Port is not ExecutePort);
                if (nonexec == null) goto normal;
                var index = stack.Children.IndexOf(nonexec);
                if (index == -1) goto normal;
                stack.Children.Insert(index, portView);
                goto end;
            }
            normal:
            stack.Children.Add(portView);
            
            
            end:
            return portView;
        }

        #region HeaderProperty

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register(nameof(Header), typeof(string), typeof(NodeView), new PropertyMetadata((d, e) => (d as NodeView)!._header = e.NewValue.ToString()!));

        [UsedImplicitly]
        private string _header = null!;
        
        public string Header
        {
            get => (string) GetValue(HeaderProperty);
            set => SetValue(HeaderProperty, value);
        }

        #endregion
        
        #region ManagerProperty

        public static readonly DependencyProperty ManagerProperty =
            DependencyProperty.Register(nameof(Manager), typeof(NodesManagerView), typeof(NodeView), new PropertyMetadata(ManagerPropertyChanged));

        private static void ManagerPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as NodeView)!._manager = (NodesManagerView) e.NewValue;
        }


        [UsedImplicitly]
        private NodesManagerView _manager = null!;
        public NodesManagerView Manager
        {
            get => (NodesManagerView) GetValue(ManagerProperty);
            set => SetValue(ManagerProperty, value);
        }

        #endregion
        
        #region PositionProperty

        public static readonly DependencyProperty PositionProperty =
            DependencyProperty.Register(nameof(Position), typeof(Vector), typeof(NodeView), new PropertyMetadata(PositionPropertyChanged));

        private static void PositionPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as NodeView)!._position = (Vector) e.NewValue;
        }


        private Vector _position;
        public Vector Position
        {
            get => (Vector) GetValue(PositionProperty);
            set
            {
                if (value == _position) return;
                Node.Position = new Vector2((float) value.X, (float) value.Y);
                _translate.X = value.X;
                _translate.Y = value.Y;
                SetValue(PositionProperty, value);
                Moved?.Invoke(value);
            }
        }

        #endregion

        #region ScaleProperty

        public static readonly DependencyProperty ScaleProperty =
            DependencyProperty.Register(nameof(Scale), typeof(Vector), typeof(NodeView), new PropertyMetadata(ScalePropertyChanged));

        private static void ScalePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as NodeView)!._scale = (Vector) e.NewValue;
        }


        private Vector _scale;
        public Vector Scale
        {
            get => (Vector) GetValue(PositionProperty);
            set
            {
                _scale.X = value.X;
                _scale.Y = value.Y;
                SetValue(PositionProperty, value);
            }
        }

        #endregion


        private void Border_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void Border_OnMouseMove(object sender, MouseEventArgs e)
        {
            
        }
    }
}