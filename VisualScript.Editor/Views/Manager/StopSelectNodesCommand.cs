﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using JetBrains.Annotations;
using VisualScript.Editor.Utils;
using VisualScript.Editor.Views.Node;

namespace VisualScript.Editor.Views.Manager
{
    public class StopSelectNodesCommand : ManagerCommand
    {
        public StopSelectNodesCommand([NotNull] NodesManagerView manager) : base(manager)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return Manager.IsSelectingNodes;
        }

        public override void Execute(object? parameter)
        {
            Manager.StartSelectNodesCommand.SelectionRect.Visibility = Visibility.Collapsed;
            Manager.IsSelectingNodes = false;

            Manager.SelectedNodes.Empty(n => n.Deselect());
            var results = new List<NodeView>();
            VisualTreeHelper.HitTest(Manager.Canvas, target =>
            {
                if (target is not NodeView nodeView) return HitTestFilterBehavior.Continue;
                results.Add(nodeView);
                return HitTestFilterBehavior.ContinueSkipChildren;
            }, _ => HitTestResultBehavior.Continue, new GeometryHitTestParameters(Manager.StartSelectNodesCommand.Geometry));
            
            foreach (var node in results)
                node.Select();
        }
    }
}