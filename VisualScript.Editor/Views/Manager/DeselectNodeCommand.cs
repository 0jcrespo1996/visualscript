﻿namespace VisualScript.Editor.Views.Manager
{
    public class DeselectNodeCommand : ManagerCommand
    {
        public DeselectNodeCommand(NodesManagerView manager) : base(manager)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return true;
        }

        public override void Execute(object? parameter)
        {
            while (Manager.SelectedNodes.Count > 0)
                Manager.SelectedNodes[0].Deselect();

            Manager.IsDraggingNodes = false;
            Manager.Canvas.Focus();
        }
    }
}