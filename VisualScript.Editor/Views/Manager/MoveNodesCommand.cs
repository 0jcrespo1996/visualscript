﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using JetBrains.Annotations;

namespace VisualScript.Editor.Views.Manager
{
    public class MoveNodesCommand : ManagerCommand
    {

        public MoveNodesCommand(NodesManagerView manager) : base(manager)
        {
     
        }
        
        public override bool CanExecute(object? parameter)
        {
            return ((Manager.SelectedNodes.Count > 1 && Keyboard.IsKeyDown(Key.LeftShift)) || (Manager.SelectedNodes.Any())) && Mouse.LeftButton == MouseButtonState.Pressed && Manager.IsMouseInside;
        }
        
        public override void Execute(object? parameter)
        {
            Manager.IsDraggingNodes = true;
            foreach (var node in Manager.SelectedNodes)
                node.Position += Manager.MouseDelta;
        }
        
    }
}