﻿using System;
using System.Windows.Input;

namespace VisualScript.Editor.Views.Manager
{
    public abstract class ManagerCommand : ICommand
    {
        public NodesManagerView Manager { get; }

        protected ManagerCommand(NodesManagerView manager)
        {
            Manager = manager;
        }
        
        public abstract bool CanExecute(object? parameter);
        
        public virtual void Execute(object? parameter)
        {
            
        }

        #pragma warning disable CS0067
        public event EventHandler? CanExecuteChanged;
        #pragma warning restore CS0067
    }
}