﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using JetBrains.Annotations;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Editor.Utils;
using VisualScript.Editor.Views.Node;
using VisualScript.Editor.Views.Port;
using VisualScript.Editor.Windows.Main;
using VisualScript.Editor.Wizards;

namespace VisualScript.Editor.Views.Manager
{
    public partial class NodesManagerView
    {

        public MoveNodesCommand MoveNodesCommand { get; }
        
        public DeselectNodeCommand DeselectNodeCommand { get; }
        
        public StartSelectNodesCommand StartSelectNodesCommand { get; }
        
        public SelectNodesCommand SelectNodesCommand { get; }
        
        public StopSelectNodesCommand StopSelectNodesCommand { get; }
        
        public ZoomCommand ZoomCommand { get; }

        public  PanCommand PanCommand { get; }
        
        public CancelConnectionCommand CancelConnectionCommand { get; }
        
        public UpdateConnectionCommand UpdateConnectionCommand { get; }
        
        public DropNodeCommand DropNodeCommand { get; }
        
        public bool IsMouseInside =>
            new Rect(0, 0, Canvas.ActualWidth, Canvas.ActualHeight).Contains(
                Mouse.GetPosition(Canvas));

        public bool IsDraggingNodes { get; set; }
        
        public bool IsSelectingNodes { get; set; }

        public Point MousePosition => Mouse.GetPosition(Canvas);
        
        public Vector MouseDelta { get; private set; }
        

        public IReadOnlyDictionary<Guid, NodeView> Nodes => _nodes;

        public ConnectionView? ActiveConnection { get; set; }
        
        public ObservableDictionary<string, object?> Globals { get; }

        public NodesManagerView()
        {
            Globals = new ObservableDictionary<string, object?>();
            Manager = new NodesManager(Globals);
            MoveNodesCommand = new MoveNodesCommand(this);
            DeselectNodeCommand = new DeselectNodeCommand(this);
            StartSelectNodesCommand = new StartSelectNodesCommand(this);
            SelectNodesCommand = new SelectNodesCommand(this);
            StopSelectNodesCommand = new StopSelectNodesCommand(this);
            ZoomCommand = new ZoomCommand(this);
            PanCommand = new PanCommand(this);
            CancelConnectionCommand = new CancelConnectionCommand(this);
            UpdateConnectionCommand = new UpdateConnectionCommand(this);
            DropNodeCommand = new DropNodeCommand(this);
            InitializeComponent();

            Canvas.MouseMove += CanvasOnMouseMove;
        }


        private readonly Dictionary<Guid, NodeView> _nodes = new();

        public NodeView AddNode(INode node)
        {
            var view = new NodeView(node, this);
            AddNode(node.Id, view);
            return view;
        }

        public void AddNode(Guid guid, NodeView view)
        {
            _nodes.Add(guid, view);
            Canvas.Children.Add(view);
        }

        public NodeView CreateNode(Type type, Point position)
        {
            INode node;
            if (type.IsDefined(typeof(RequiresWizard)))
            {
                if (!Wizard.TryGetWizard(type, out var wizard))
                    throw new ApplicationException($"Cannot get wizard for {type.FullName ?? type.Name}");
                
                if (!wizard.TrySetup(this, out node, out var errorMessage))
                    throw new ApplicationException($"Wizard failed to setup node {type.Name}", errorMessage);
            }
            else node = Manager.CreateNode(type, Guid.NewGuid());
            
            var uiNode = AddNode(node);
            uiNode.Loaded += (_, _) =>
            {
                var mousePos = position;
                mousePos = new Point(mousePos.X - (uiNode.ActualWidth / 2), mousePos.Y - (uiNode.ActualHeight / 2));
                uiNode.Position = new Vector(mousePos.X, mousePos.Y);
            };
            return uiNode;
        }

        public void DeleteNode(NodeView node)
        {
            SelectedNodes.Remove(node);
            _nodes.Remove(node.Node.Id);
            Canvas.Children.Remove(node);
            node.Remove();
            Manager.RemoveNode(node.Node);
        }

        public void DeleteSelected()
        {
            SelectedNodes.Empty(DeleteNode);
        }
        

        private Point _prevPos;
        private void CanvasOnMouseMove(object sender, MouseEventArgs e)
        {
            var pos = e.GetPosition(Canvas);
            MouseDelta = pos - _prevPos;
            _prevPos = pos;
        }


        #region MainWindowProperty

        public static readonly DependencyProperty MainWindowProperty =
            DependencyProperty.Register(nameof(MainWindow), typeof(MainWindow), typeof(NodesManagerView), new PropertyMetadata((d, e) => (d as NodesManagerView)!._mainWindow = (MainWindow) e.NewValue));
        
        [UsedImplicitly]
        private MainWindow _mainWindow = null!;
        public MainWindow MainWindow { get => (MainWindow) GetValue(MainWindowProperty); set => SetValue(MainWindowProperty, value); }

        #endregion
        
        #region SelectedNodesProperty


        public ObservableCollection<NodeView> SelectedNodes { get; } = new();

        #endregion
        
        #region ManagerProperty
        public static readonly DependencyProperty ManagerProperty =
            DependencyProperty.Register(nameof(Manager),
                typeof(NodesManager),
                typeof(NodesManagerView), new PropertyMetadata(ManagerPropertyChanged));

        private static void ManagerPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as NodesManagerView)!._manager = (NodesManager) e.NewValue;
        }

        [UsedImplicitly]
        private NodesManager _manager = null!;
        public NodesManager Manager { get => (NodesManager) GetValue(ManagerProperty);
            private init => SetValue(ManagerProperty, value);
        }
        
        #endregion


        public void Clear()
        {
            Manager.Clear();
            var copy = _nodes.Values.ToArray();
            foreach (var node in copy)
                DeleteNode(node);
            
            Globals.Clear();
        }

        public PortView? FindPort(Guid id)
        {
            foreach (var node in _nodes.Values)
                if (node.InputPorts.TryGetValue(id, out var port) || node.OutputPorts.TryGetValue(id, out port))
                    return port;
            return null;
        }

        public void Deserialize(byte[] data)
        {
            Clear();
            Manager.Clear();
            Manager.Deserialize(data);
            var connectedPorts = new List<Guid>();

            foreach (var node in Manager.Nodes)
            {
                if (_nodes.ContainsKey(node.Id)) continue;
                AddNode(node);
            }
            
            
            foreach (var uiNode in Nodes.Values)
            {
                foreach (var port in uiNode.Node.InputPorts.Concat(uiNode.Node.OutputPorts))
                {
                    if (port.Connection == null || connectedPorts.Contains(port.Id)) continue;
                    var pair = FindPort(port.Connection.Value.GetPair(port).Id)!;
                    var uiPort = FindPort(port.Id)!;
                    var connection = new ConnectionView(uiPort);
                    connection.ConnectTo(pair);
                    connectedPorts.Add(pair.Port.Id);
                    connectedPorts.Add(uiPort.Port.Id);
                    connection.Update();
                }
            }
        }
    }
}