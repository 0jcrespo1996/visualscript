﻿using System;
using System.Windows;
using System.Windows.Input;
using JetBrains.Annotations;

namespace VisualScript.Editor.Views.Manager
{
    public class UpdateConnectionCommand : ManagerCommand
    {
        public UpdateConnectionCommand([NotNull] NodesManagerView manager) : base(manager)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return Manager.ActiveConnection != null && Mouse.LeftButton == MouseButtonState.Pressed;
        }

        public override void Execute(object? parameter)
        {
            var e = (MouseEventArgs) parameter!;

            var mousePos = Manager.MousePosition;
            Manager.ActiveConnection!.Update(new Vector(mousePos.X, mousePos.Y));
            e.Handled = true;
        }
    }
}