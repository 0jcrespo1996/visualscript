﻿using System.Windows.Input;
using JetBrains.Annotations;

namespace VisualScript.Editor.Views.Manager
{
    public class PanCommand : ManagerCommand
    {
        public PanCommand([NotNull] NodesManagerView manager) : base(manager)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            
            var args = parameter as MouseEventArgs;
            return args!.MiddleButton == MouseButtonState.Pressed;
        }

        public override void Execute(object? parameter)
        {
            
            foreach (var node in Manager.Nodes.Values)
            {
                node.Position += Manager.MouseDelta;
            }
        }
    }
}