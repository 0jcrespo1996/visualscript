﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using JetBrains.Annotations;

namespace VisualScript.Editor.Views.Manager
{
    public class ZoomCommand : ManagerCommand
    {
        public ZoomCommand([NotNull] NodesManagerView manager) : base(manager)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return Manager.IsMouseInside;
        }

        public override void Execute(object? parameter)
        {
            var args = parameter as MouseWheelEventArgs;
            
            var pos = Manager.MousePosition;
            var scale = Manager.Scale;
            
            if (args!.Delta != 0)
            {
                scale.CenterX = pos.X;
                scale.CenterY = pos.Y;
                args.Handled = true;
            }

            if (args.Delta > 0)
            {
                scale.ScaleX *= 2;
                scale.ScaleY *= 2;
            }
            else
            {
                scale.ScaleX /= 2;
                scale.ScaleY /= 2;
            }
        }
        
    }
}