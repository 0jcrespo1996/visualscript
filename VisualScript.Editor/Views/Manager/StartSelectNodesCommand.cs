﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using JetBrains.Annotations;

namespace VisualScript.Editor.Views.Manager
{
    public class StartSelectNodesCommand : ManagerCommand
    {
        
        public Path SelectionRect { get; }
        public RectangleGeometry Geometry { get; }
        
        public Vector StartPosition { get; private set; }
        
        public StartSelectNodesCommand([NotNull] NodesManagerView manager) : base(manager)
        {
            SelectionRect = new Path() {Fill = Brushes.Black, Opacity = 0.2, IsHitTestVisible = false, Visibility = Visibility.Collapsed};
            Panel.SetZIndex(SelectionRect, 1);
            Manager.Initialized += (_ , _) => Manager.Canvas.Children.Add(SelectionRect);
            Geometry = new RectangleGeometry(new Rect(0, 0, 512, 512));
            SelectionRect.Data = Geometry;
        }

        public override bool CanExecute(object? parameter)
        {
            return Manager.Canvas.IsFocused && !Manager.MainWindow.IsDragging && Manager.ActiveConnection == null && Mouse.LeftButton == MouseButtonState.Pressed && !Manager.IsSelectingNodes && Manager.SelectedNodes.Count == 0;
        }

        public override void Execute(object? parameter)
        {
            Manager.IsSelectingNodes = true;
            SelectionRect.Visibility = Visibility.Visible;
            var pos = Mouse.GetPosition(Manager.Canvas);
            StartPosition = new Vector(pos.X, pos.Y);
            Geometry.Rect = new Rect(StartPosition.X, StartPosition.Y, 512, 512);
        }
    }
}