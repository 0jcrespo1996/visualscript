﻿using System;
using System.Windows;
using System.Windows.Controls;
using JetBrains.Annotations;
using Serilog;
using VisualScript.Editor.Utils;
using VisualScript.Editor.Views.Node;

namespace VisualScript.Editor.Views.Manager
{
    public class DropNodeCommand : ManagerCommand
    {
        public DropNodeCommand([NotNull] NodesManagerView manager) : base(manager)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return true;
        }

        public override void Execute(object? parameter)
        {
            var e = (DragEventArgs) parameter!;

            var data = (e.Data.GetData(typeof(NodeMeta)) as TreeViewItem)?.Header as NodeMeta?;
            if (data == null) return;
            var type = data.Value.Type;
            try
            {
                Manager.CreateNode(type, e.GetPosition(Manager.Canvas));
            }
            catch (Exception exception)
            {
                if (exception.InnerException is OperationCanceledException)
                    return;
                Log.Error("{Message}",exception.Message);
            }
        }
    }
}