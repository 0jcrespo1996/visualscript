﻿using System;
using System.Windows.Input;
using JetBrains.Annotations;

namespace VisualScript.Editor.Views.Manager
{
    public class CancelConnectionCommand : ManagerCommand
    {
        public CancelConnectionCommand([NotNull] NodesManagerView manager) : base(manager)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return Manager.ActiveConnection != null;
        }

        public override void Execute(object? parameter)
        {
            var e = (MouseEventArgs) parameter!;

            Manager.ActiveConnection?.Discard();

            e.Handled = true;
        }
    }
}