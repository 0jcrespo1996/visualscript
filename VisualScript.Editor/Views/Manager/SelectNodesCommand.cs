﻿using System.Windows;
using System.Windows.Input;
using JetBrains.Annotations;

namespace VisualScript.Editor.Views.Manager
{
    public class SelectNodesCommand : ManagerCommand
    {
        public SelectNodesCommand([NotNull] NodesManagerView manager) : base(manager)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return Manager.IsSelectingNodes;
        }

        public override void Execute(object? parameter)
        {
            var pos = Mouse.GetPosition(Manager.Canvas);

            var start = Manager.StartSelectNodesCommand;
            var startPos = start.StartPosition;


            double x, width, y, height;
            if (startPos.X < pos.X)
            {
                x = startPos.X;
                width = pos.X - startPos.X;
            }
            else
            {
                x = pos.X;
                width = startPos.X - pos.X;
            }

            if (startPos.Y < pos.Y)
            {
                y = startPos.Y;
                height = pos.Y - startPos.Y;
            }
            else
            {
                y = pos.Y;
                height = startPos.Y - pos.Y;
            }

            start.Geometry.Rect = new Rect(x, y, width, height);
        }
    }
}