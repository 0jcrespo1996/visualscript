﻿using System.Windows.Input;
using System.Windows.Media;
using JetBrains.Annotations;

namespace VisualScript.Editor.Views.Port
{
    public class CheckConnectEndCommand : PortCommand
    {
        public CheckConnectEndCommand([NotNull] PortView view) : base(view)
        {
            
        }

        public override bool CanExecute(object? parameter)
        {
            return View.Node.Manager.ActiveConnection != null;
        }


        public override void Execute(object? parameter)
        {
            var e = (MouseEventArgs) parameter!;

            View.Node.Manager.ActiveConnection!.Line.Stroke = Brushes.White;
            
            e.Handled = true;
        }
    }
}