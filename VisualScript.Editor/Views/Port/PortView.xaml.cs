﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.Editor.Editors.Ports;
using VisualScript.Editor.Views.Node;

namespace VisualScript.Editor.Views.Port
{
    public partial class PortView
    {
        public IPort Port { get; }
        
        public NodeView Node { get; }
        
        public Shape Shape { get; }
        
        public ConnectionView? Connection { get; set; }
        
        public ConnectionStartCommand ConnectionStartCommand { get; }
        
        public ConnectionFinishCommand ConnectionFinishCommand { get; }
        
        public CheckConnectCommand CheckConnectCommand { get; }
        
        public CheckConnectEndCommand CheckConnectEndCommand { get; }
        
        public PortEditor? Editor { get; }
        
        public PortView(NodeView node, IPort port)
        {
            Node = node;
            Port = port;
            ConnectionStartCommand = new ConnectionStartCommand(this);
            ConnectionFinishCommand = new ConnectionFinishCommand(this);
            CheckConnectCommand = new CheckConnectCommand(this);
            CheckConnectEndCommand = new CheckConnectEndCommand(this);
            InitializeComponent();
            NameLabel.Content = port.Name;
            ToolTip = port.Description;
            Shape = GetShape();
            ShapeGrid.Children.Add(Shape);
            Margin = new Thickness(Shape.Margin.Left, 0, 0, 5);
            Node.Moved += (_) => Connection?.Update();
            Editor = TryGetEditor();

            if (Editor != null && !port.IsInput && Node.Node.InputPorts.OfType<IValuePort>().Any())
                Editor.Disable();
        }

        private PortEditor? TryGetEditor()
        {
            if (Port is not IValuePort valuePort) return null;
            var type = valuePort.ValueType;
            var prop = Port.Node.GetType().GetProperty(Port.PropertyName, BindingFlags.Public | BindingFlags.Instance);
            if (prop == null) return null;
            Type? editorType;

            if (prop!.IsDefined(typeof(EnumerateAttribute)))
                PortEditor.TryGetEnumerator(type, Port.Node.GetType(), prop.Name, out editorType);
            else
                PortEditor.TryGetEditor(type, out editorType);

            if (editorType == null) return null;
            var instance =
                Activator.CreateInstance(editorType, this, Port, Port.IsInput ? Node.LeftStack : Node.RightStack) as PortEditor;
            
            instance?.Enable();
            return instance;
        }


        private Shape GetShape()
        {
            Shape shape = Port switch
            {
                
                ExecutePort => new Polygon()
                {
                    Points =
                    {
                        new Point(0, 0),
                        new Point(8, 0),
                        new Point(16, 8),
                        new Point(8, 16),
                        new Point(0, 16)
                    },
                    Margin = new Thickness(1, 7, 0, 0)
                },
                IValuePort => new Ellipse() {Width = 12, Height = 12, Margin = new Thickness(4, 0, 0, 0)},
                _ => throw new InvalidOperationException()
            };

            
            
            shape.RenderTransform = new ScaleTransform(-1, 1);
            shape.RenderTransformOrigin = new Point(0.5, 0.5);
            shape.StrokeThickness = 1;
            shape.Fill = Brushes.Transparent;
            shape.Stroke = new SolidColorBrush(Color.FromArgb(Port.Color.A, Port.Color.R, Port.Color.G, Port.Color.B));
            return shape;
        }
    }
}