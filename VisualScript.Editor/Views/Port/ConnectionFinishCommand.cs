﻿using System.Windows.Input;
using JetBrains.Annotations;

namespace VisualScript.Editor.Views.Port
{
    public class ConnectionFinishCommand : PortCommand
    {
        public ConnectionFinishCommand([NotNull] PortView view) : base(view)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return View.Node.Manager.ActiveConnection != null;
        }

        public override void Execute(object? parameter)
        {
            var e = (MouseEventArgs) parameter!;

            View.Node.Manager.ActiveConnection?.ConnectTo(View);
            
            e.Handled = true;
        }
    }
}