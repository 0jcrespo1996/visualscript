﻿using System;
using System.Windows.Input;

namespace VisualScript.Editor.Views.Port
{
    public abstract class PortCommand : ICommand
    {
        public PortView View { get; }

        protected PortCommand(PortView view)
        {
            View = view;
        }
        
        public abstract bool CanExecute(object? parameter);

        public virtual void Execute(object? parameter)
        {
            
        }

#pragma warning disable CS0067
        public event EventHandler? CanExecuteChanged;
#pragma warning restore CS0067
    }
}