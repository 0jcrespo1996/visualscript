﻿using System.Windows.Input;
using System.Windows.Media;
using JetBrains.Annotations;

namespace VisualScript.Editor.Views.Port
{
    public class CheckConnectCommand : PortCommand
    {
        public CheckConnectCommand([NotNull] PortView view) : base(view)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return View.Node.Manager.ActiveConnection != null;
        }

        public override void Execute(object? parameter)
        {
            var e = (MouseEventArgs) parameter!;
            var connection = View.Node.Manager.ActiveConnection!;
            if (connection.From.Port.CanConnectTo(View.Port))
                connection.Line.Stroke = Brushes.Green;
            else connection.Line.Stroke = Brushes.Red;
            e.Handled = true;
        }
    }
}