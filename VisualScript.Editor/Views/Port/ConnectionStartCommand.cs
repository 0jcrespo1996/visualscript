﻿using System;
using System.Windows.Input;
using JetBrains.Annotations;
using VisualScript.Editor.Utils;

namespace VisualScript.Editor.Views.Port
{
    public class ConnectionStartCommand : PortCommand
    {
        public ConnectionStartCommand([NotNull] PortView view) : base(view)
        {
        }

        public override bool CanExecute(object? parameter)
        {
            return true;
        }

        public override void Execute(object? parameter)
        {
            var e = (MouseEventArgs) parameter!;
            View.Node.Manager.SelectedNodes
                .Empty(n => n.Deselect());
            e.Handled = true;
            View.Connection?.Disconnect();
            View.Node.Manager.ActiveConnection = new ConnectionView(View);
        }
    }
}