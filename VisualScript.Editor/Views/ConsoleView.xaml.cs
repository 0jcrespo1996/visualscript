﻿using System;
using System.Windows.Controls;
using VisualScript.Editor.Utils;

namespace VisualScript.Editor.Views
{
    public partial class ConsoleView : UserControl
    {
        public TextBoxWriter Writer { get; }
        public ConsoleView()
        {
            InitializeComponent();
            Writer = new TextBoxWriter(Output);
            Console.SetOut(Writer);
        }
    }
}