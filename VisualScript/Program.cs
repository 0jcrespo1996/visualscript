﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Serilog;
using VisualScript.Core;

namespace VisualScript
{
    class Program
    {
        
        static async Task Main(string[] args)
        {
            var file = args.ElementAtOrDefault(0);
            if (string.IsNullOrWhiteSpace(file) || !File.Exists(file)) return;
            var manager = new NodesManager();
            Log.Information("Visual Script V{Version}", Assembly.GetExecutingAssembly().GetName().Version);
            _ = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll").Concat(Directory.GetFiles(Environment.CurrentDirectory, "*.dll").Distinct()).Where(f =>
            {
                try
                {
                    AssemblyName.GetAssemblyName(f);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }).Select(Assembly.LoadFrom);
            var bytes = await File.ReadAllBytesAsync(file);
            manager.Deserialize(bytes);
            var entryNode = manager.GetEntryNode();
            if (entryNode == null)
            {
                Log.Error("File does not have an entry node");
                return;
            }
            

            await entryNode.RunAsync(new Context());

        }
    }
}