﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.CoreNodes.Resources;

namespace VisualScript.CoreNodes.Utils
{
    public interface ISplitterNode : INode
    {
        IValuePort Add();
        IPort? Pop();
    }
    
    [Node("SplitterNode", typeof(CoreStrings))]
    [RequiresWizard]
    [Category(typeof(CoreStrings), "Core", "Utils")]
    public class SplitterNode <T> : ExecuteNode, ISplitterNode
    {
        [Port("ValueNode", true, typeof(CoreStrings))]
        public ValuePort<T> Input { get; private set; } = null!;
        
        public SplitterNode(Guid id, NodesManager manager) : base(id, manager)
        {
        }

        public override async Task<IList<ExecutePort>?> OnExecuteAsync(IPort executor, Context context)
        {
            var value = await Input.GetValueAsync(context);
            foreach (var port in OutputPorts.OfType<ValuePort<T>>())
                context.SetResult(port, value);
            return await base.OnExecuteAsync(executor, context);
        }

        

        public IValuePort Add()
        {
            var port = new ValuePort<T>(this, Guid.NewGuid(), string.Empty, string.Empty, false);
            AddPort(port);
            Manager.AddPort(port);
            return port;
        }


        public IPort? Pop()
        {
            var list = _outputPorts;
            var last = list.FirstOrDefault(p => p is ValuePort<T>);
            if (last == null) return null;
            list.Remove(last);
            Manager.RemovePort(last);
            return last;
        }
    }
}