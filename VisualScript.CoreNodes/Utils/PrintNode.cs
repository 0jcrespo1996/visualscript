﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Serilog;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.CoreNodes.Resources;

namespace VisualScript.CoreNodes.Utils
{
    [Node("PrintNodeName",  typeof(CoreStrings))]
    [Entity("B3BBF272-7CDE-4508-9FCC-05588DF21596")]
    [Description("PrintNodeDescription",  typeof(CoreStrings))]
    [Category(typeof(CoreStrings), "Core", "Utils")]
    public class PrintNode : ExecuteNode
    {
        [Port("NodeValue", true,  typeof(CoreStrings))]
        [Description("PrintNodeValueDesc",  typeof(CoreStrings))]
        public ValuePort<object> Value { get; private set; } = null!;

        public PrintNode(Guid id, NodesManager manager) : base(id, manager)
        {
        }

        public override async Task<IList<ExecutePort>?> OnExecuteAsync(IPort executor, Context context)
        {
            var value = await Value.GetValueAsync(context);
            Log.Information("{Value}", value?.ToString() ?? "null");
            return await base.OnExecuteAsync(executor, context);
        }
        
    }
}