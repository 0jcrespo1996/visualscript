﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.CoreNodes.Resources;

namespace VisualScript.CoreNodes.Utils
{
    [Node("AwaitNode", typeof(CoreStrings))]
    [Description("AwaitNodeDesc", typeof(CoreStrings))]
    [Entity("07D6DBAE-DA09-43B8-BD67-DA8975A938ED")]
    [Category(typeof(CoreStrings), "Core", "Utils")]
    public class AwaitNode : ExecuteNode
    {
        [Port("AwaitTime", true, typeof(CoreStrings))]
        [Description("AwaitTimeDesc", typeof(CoreStrings))]
        public ValuePort<Number> Time { get; private set; } = null!;
        
        public AwaitNode(Guid id, NodesManager manager) : base(id, manager)
        {
        }

        public override async Task<IList<ExecutePort>?> OnExecuteAsync(IPort executor, Context context)
        {
            var time = await Time.GetValueAsync(context);
            await Task.Delay(time);
            return await base.OnExecuteAsync(executor, context);
        }
    }
}