﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.CoreNodes.Resources;

namespace VisualScript.CoreNodes.Utils
{
    [Node("SwitcherNode", typeof(CoreStrings))]
    [Category(typeof(CoreStrings), "Core", "Utils")]
    public class SwitcherNode : ExecuteNode
    {
        public SwitcherNode(Guid id, NodesManager manager) : base(id, manager)
        {
        }


        public override Task<IList<ExecutePort>?> OnExecuteAsync(IPort executor, Context context)
        {
            return Task.FromResult<IList<ExecutePort>?>(OutputPorts.OfType<ExecutePort>().Reverse().ToArray());
        }
        

        public ExecutePort Add()
        {
            var port = new ExecutePort(this, Guid.NewGuid(), string.Empty, string.Empty, false);
            AddPort(port);
            Manager.AddPort(port);
            return port;
        }


        public IPort? Pop()
        {
            var list = _outputPorts;
            var last = list.FirstOrDefault();
            if (last == null) return null;
            list.Remove(last);
            Manager.RemovePort(last);
            return last;
        }
    }
}