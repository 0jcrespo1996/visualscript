﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.CoreNodes.Resources;

namespace VisualScript.CoreNodes.Utils
{
    [Node("CastNodeName",  typeof(CoreStrings))]
    [Description("CastNodeDescription",  typeof(CoreStrings))]
    [RequiresWizard]
    [Category(typeof(CoreStrings), "Core", "Utils")]
    public class CastNode <T> : ExecuteNode
    {
        [Port("NodeValue", true,  typeof(CoreStrings))]
        [Description("CastNodeValueDesc",  typeof(CoreStrings))]
        public ValuePort<object> Value { get; private set; } = null!;

        [Port("NodeValue", false, typeof(CoreStrings))]
        [Description("CastOutPortDesc", typeof(CoreStrings))]
        public ValuePort<T> Output { get; private set; } = null!;

        [Port("Error", false, typeof(CoreStrings))]
        public ExecutePort Error { get; private set; } = null!;

        public CastNode(Guid id, NodesManager manager) : base(id, manager)
        {
        }

        
        public override async Task<IList<ExecutePort>?> OnExecuteAsync(IPort executor, Context context)
        {
            var value = await Value.GetValueAsync(context);
            if (!VisualHelpers.TryCastTo(value, typeof(T), out var result)) return new[] {Error};
            context.SetResult(Output, (T) result!);
            return await base.OnExecuteAsync(executor, context);

        }
        
    }
}