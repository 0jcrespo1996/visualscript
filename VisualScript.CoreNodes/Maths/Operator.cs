﻿using System.ComponentModel;

namespace VisualScript.CoreNodes.Maths
{
    public enum Operator
    {
        [Description("+")]
        Plus,
        [Description("-")]
        Minus,
        [Description("x")]
        Multiply,
        [Description("÷")]
        Divide,
        [Description("%")]
        Modulus,
        [Description("**")]
        Power
    }
}