﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.CoreNodes.Resources;

namespace VisualScript.CoreNodes.Maths
{
    [Node("ToNumberNode", typeof(CoreStrings))]
    [Description("ToNumberDescription", typeof(CoreStrings))]
    [Category(typeof(CoreStrings), "Core", "Maths")]
    public class ToNumberNode : ExecuteNode
    {
        [Port("NodeValue", true, typeof(CoreStrings))]
        public ValuePort<string> Input { get; private set; } = null!;

        [Port("Result", false, typeof(CoreStrings))]
        public ValuePort<Number> Result { get; private set; } = null!;

        [Port("Error")] public ExecutePort Error { get; private set; } = null!;
        
        public ToNumberNode(Guid id, NodesManager manager) : base(id, manager)
        {
        }

        public override async Task<IList<ExecutePort>?> OnExecuteAsync(IPort executor, Context context)
        {
            var input = await Input.GetValueAsync(context);
            
            if (input == null || !Number.TryParse(input, out var number))
                return new[] {Error};
            
            context.SetResult(Result, number);
            return await base.OnExecuteAsync(executor, context);
        }
    }
}