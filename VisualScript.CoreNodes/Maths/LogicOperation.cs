﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.CoreNodes.Resources;

namespace VisualScript.CoreNodes.Maths
{
    
        [Node("LogicOperation")]
        [Category(typeof(CoreStrings), "Core", "Maths")]
        public class LogicOperation : ExecuteNode
        {
            [Port("A", true)] public ValuePort<object> A { get; private set; } = null!;

            [Port("LogicalOperator", true, typeof(CoreStrings))]
            public ValuePort<string> Operator { get; private set; } = null!;

            [Port("B", true)] public ValuePort<object> B { get; private set; } = null!;

            [Port("Result", false, typeof(CoreStrings))]
            public ValuePort<bool> Result { get; private set; } = null!;
            public LogicOperation(Guid id, NodesManager manager) : base(id, manager)
            {
            }


            public override async Task<IList<ExecutePort>?> OnExecuteAsync(IPort executor, Context context)
            {
                var left = await A.GetValueAsync(context);
                var op = await Operator.GetValueAsync(context);
                var right = await B.GetValueAsync(context);

                object? res = null;

                if (left is Number numleft && right is Number numRight)
                {
                    res = op switch
                    {
                        ">" => numleft > numRight,
                        ">=" => numleft >= numRight,
                        "<" => numleft < numRight,
                        "<=" => numleft <= numRight,
                        "==" => numleft == numRight,
                        "!=" => numleft != numRight,
                        _ => throw new InvalidOperationException($"Cannot use operator {op} with numbers")
                    };
                }

                if (op == "==" && res == null) res = (left == right || (left?.Equals(right) ?? false));
                else if (op == "!=" && res == null) res = (left != right || (!left?.Equals(right) ?? false));
                else if (res == null) throw new InvalidOperationException($"Cannot use operator {op} {left} and {right}");
                
                context.SetResult(Result, res);
                return await base.OnExecuteAsync(executor, context);
            }
        }
    
}