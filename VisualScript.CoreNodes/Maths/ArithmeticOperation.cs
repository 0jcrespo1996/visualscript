﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.CoreNodes.Resources;

namespace VisualScript.CoreNodes.Maths
{
    [Node("ArithmeticNodeName", typeof(CoreStrings))]
    [Description("ArithmeticNodeDescription", typeof(CoreStrings))]
    [Category(typeof(CoreStrings), "Core", "Maths")]
    public class ArithmeticOperation : ExecuteNode
    {
        [Port("A", true)] public ValuePort<Number> A { get; private set; } = null!;

        [Port("ArithmeticOperator", true, typeof(CoreStrings))]
        public ValuePort<Operator> Operator { get; private set; } = null!;

        [Port("B", true)] public ValuePort<Number> B { get; private set; } = null!;

        [Port("Result", false, typeof(CoreStrings))]
        public ValuePort<Number> Result { get; private set; } = null!;
        public ArithmeticOperation(Guid id, NodesManager manager) : base(id, manager)
        {
        }


        public override async Task<IList<ExecutePort>?> OnExecuteAsync(IPort executor, Context context)
        {
            var left = await A.GetValueAsync(context);
            var op = await Operator.GetValueAsync(context);
            var right = await B.GetValueAsync(context);

            
            var res = op switch
            {
                Maths.Operator.Plus => left + right,
                Maths.Operator.Minus => left - right,
                Maths.Operator.Multiply => left * right,
                Maths.Operator.Divide => left / right,
                Maths.Operator.Modulus => left % right,
                Maths.Operator.Power => new Number(Math.Pow(left, right)),
                _ => throw new InvalidOperationException($"'{op}' operator is not supported")
            };

            if (!double.IsFinite(res))
                throw new NotFiniteNumberException(res);
            
            context.SetResult(Result, res);
            return await base.OnExecuteAsync(executor, context);
        }
    }
}