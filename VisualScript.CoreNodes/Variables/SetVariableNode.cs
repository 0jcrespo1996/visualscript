﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.CoreNodes.Resources;

namespace VisualScript.CoreNodes.Variables
{
    [Node("SetVariableNode", typeof(CoreStrings))]
    [Entity("210B4CD7-E49C-4270-9DD9-BAD235CD7FB2")]
    [Description("SetVariableNodeDesc", typeof(CoreStrings))]
    [Category(typeof(CoreStrings), "Core", "Variables")]
    public class SetVariableNode : ExecuteNode
    {
        [Port("Name", true, typeof(CoreStrings))]
        [Enumerate]
        public ValuePort<string> VarName { get; private set; } = null!;

        [Port("Value", true, typeof(CoreStrings))]
        public ValuePort<object?> InputValue { get; private set; } = null!;

        [Port("Value", false, typeof(CoreStrings))]
        public ValuePort<object?> Value { get; private set; } = null!;
        
        public SetVariableNode(Guid id, NodesManager manager) : base(id, manager)
        {
        }

        public override async Task<IList<ExecutePort>?> OnExecuteAsync(IPort executor, Context context)
        {
            var name = await VarName.GetValueAsync(context);
            var value = await InputValue.GetValueAsync(context);
            Manager.SetVariable(name!, value);
            context.SetResult(Value, value);
            return await base.OnExecuteAsync(executor, context);
        }
    }
}