﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.CoreNodes.Resources;

namespace VisualScript.CoreNodes.Variables
{
    [Node("GetVariableNode", typeof(CoreStrings))]
    [Entity("FF098A3B-05F1-465F-91BC-C1FDB3C33312")]
    [Description("GetVariabelNodeDesc", typeof(CoreStrings))]
    [Category(typeof(CoreStrings), "Core", "Variables")]
    public class GetVariableNode : ExecuteNode
    {
        [Port("Name", true, typeof(CoreStrings))]
        [Enumerate]
        public ValuePort<string> VarName { get; private set; } = null!;

        [Port("Value", false, typeof(CoreStrings))]
        public ValuePort<object?> Value { get; private set; } = null!;
        
        public GetVariableNode(Guid id, NodesManager manager) : base(id, manager)
        {
        }

        public override async Task<IList<ExecutePort>?> OnExecuteAsync(IPort executor, Context context)
        {
            var name = await VarName.GetValueAsync(context);
            var value = Manager.GetVariable(name!);
            context.SetResult(Value, value);
            return await base.OnExecuteAsync(executor, context);
        }
    }
}