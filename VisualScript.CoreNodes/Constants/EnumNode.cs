﻿using System;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.CoreNodes.Resources;

namespace VisualScript.CoreNodes.Constants
{
    [Node("EnumNode", typeof(CoreStrings))]
    [Entity("166CF983-05BF-4EDE-9115-12F497B56ADA")]
    [Category(typeof(CoreStrings), "Core", "Constants")]
    [RequiresWizard]
    public class EnumNode <T> : Node where T : Enum
    {

        [Port("NodeValue", false, typeof(CoreStrings))]
        [Description("ReturnedValue", typeof(CoreStrings))]
        public ValuePort<T> Value { get; private set; } = null!;
        
        public EnumNode(Guid id, NodesManager manager) : base(id, manager)
        {
        }

        public override void OnInit()
        {
            Value.ConstantValue = default;
        }
    }
}