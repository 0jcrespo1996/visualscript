﻿using System;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.CoreNodes.Resources;

namespace VisualScript.CoreNodes.Constants
{
    [Node("NumberNode", typeof(CoreStrings))]
    [Entity("07091218-40DD-4EDE-ACCF-9387AFBDD3A1")]
    [Category(typeof(CoreStrings), "Core", "Constants")]
    public class NumberNode : Node
    {
        [Port("NodeValue", false, typeof(CoreStrings))]
        [Description("ReturnedValue", typeof(CoreStrings))]
        public ValuePort<Number> Output { get; private set; } = null!;
        
        public NumberNode(Guid id, NodesManager manager) : base(id, manager)
        {
        }

        public override void OnInit()
        {
            Output.ConstantValue = 0;
        }
    }
}