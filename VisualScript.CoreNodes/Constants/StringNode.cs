﻿using System;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.Core.Serialization;
using VisualScript.CoreNodes.Resources;

namespace VisualScript.CoreNodes.Constants
{
    [Node("StringNode", typeof(CoreStrings))]
    [Entity("EB1FAF72-929A-485F-896A-14C093730C04")]
    [Category(typeof(CoreStrings), "Core", "Constants")]
    public class StringNode : Node
    {
        [Port("NodeValue", false, typeof(CoreStrings))]
        [Description("ReturnedValue", typeof(CoreStrings))]
        public ValuePort<string> Output { get; private set; } = null!;
        
        public StringNode(Guid id, NodesManager manager) : base(id, manager)
        {
        }

        public override void OnInit()
        {
            Output.ConstantValue = string.Empty;
        }
    }
}