﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.CoreNodes.Resources;

namespace VisualScript.CoreNodes
{
    public abstract class EventNode <T> : Node
    {
        
        [Port("")] public ExecutePort Exit { get; protected set; } = null!;
        
        [Port("NodeValue", false, typeof(CoreStrings))]
        public ValuePort<T> Output { get; private set; } = null!;
        
        protected EventNode(Guid id, NodesManager manager) : base(id, manager)
        {
        }

        public override Task<IList<ExecutePort>?> OnExecuteAsync(IPort executor, Context context)
        {
            return Task.FromResult<IList<ExecutePort>?>(new[] {Exit});
        }
    }
}