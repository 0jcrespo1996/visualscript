﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VisualScript.Core;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.Core.Serialization;

namespace VisualScript.CoreNodes
{
    public abstract class ExecuteNode : Node
    {
        [Port("",isInput: true)] public ExecutePort Entrance { get; protected set; } = null!;

        [Port("")] public ExecutePort Exit { get; protected set; } = null!;
        
        protected ExecuteNode(Guid id, NodesManager manager) : base(id, manager)
        {
        }

        public override Task<IList<ExecutePort>?> OnExecuteAsync(IPort executor, Context context)
        {
            return Task.FromResult<IList<ExecutePort>?>(new[] {Exit});
        }
        
    }
}