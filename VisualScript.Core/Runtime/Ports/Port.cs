﻿using System;
using System.Drawing;
using System.Reflection;
using VisualScript.Core.Attributes;
using VisualScript.Core.Resources;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Core.Serialization;

namespace VisualScript.Core.Runtime.Ports
{
    public abstract class Port : IPort
    {
        public Guid Id { get; private set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public Color Color { get; set; }
        public bool IsInput { get; private set; }

        public Connection? Connection { get;  set; }

        public INode Node { get; private set; } = null!;

        public string PropertyName { get; private set; } = null!;

        private bool _hasConnection;

        protected Port(Guid id)
        {
            Id = id;
            Color = GetType().GetCustomAttribute<ColorAttribute>()?.Color ?? Color.White;
        }

        protected Port(INode node, Guid id, string name, string propertyName, bool isInput) : this(id)
        {
            Name = name;
            IsInput = isInput;
            Node = node;
            PropertyName = propertyName;
            var type = Node.GetType();
            var prop = type.GetProperty(PropertyName);
            var color = prop?.GetCustomAttribute<ColorAttribute>()?.Color;
            if (color == null && this is IValuePort valuePort)
                color = ColorAttribute.ForValuePort(valuePort);
            else color = Color.White;
            Color = color.Value;
        }
        
        public virtual bool CanConnectTo(IPort port)
        {
            return IsInput != port.IsInput && Id != port.Id && Node.Id != port.Node.Id;
        }

        public virtual void ConnectTo(IPort port)
        {
            if (!CanConnectTo(port))
                throw new InvalidOperationException(string.Format(strings.CannotConnectPort, GetType(), port.GetType()));

            if (Connection?.To.Id == port.Id || Connection?.From.Id == port.Id) return;
            Connection = new Connection(this, port);
            port.Connection = Connection;
            _hasConnection = true;
            if (port is Port p) p._hasConnection = true;
        }

        public virtual void Disconnect()
        {
            if (Connection == null) return;
            var conn = Connection.Value;
            conn.From.Connection = null;
            conn.To.Connection = null;
            if (conn.From is Port p) p._hasConnection = false;
            if (conn.To is Port p2) p2._hasConnection = false;
        }

        public virtual void Serialize(Serializer serializer)
        {
            
            serializer.Write(Id);
            
            serializer.WriteString(Name);
            
            serializer.WriteString(PropertyName);
            
            serializer.Write(!string.IsNullOrWhiteSpace(Description));
            
            if (!string.IsNullOrWhiteSpace(Description))
                serializer.WriteString(Description);
            
            serializer.Write(Color.ToArgb());
            serializer.Write(IsInput);
            serializer.Write(Connection != null);
            if (Connection != null)
            {
                serializer.Write(Connection.Value.From.Id);
                serializer.Write(Connection.Value.To.Id);   
            }
            serializer.Write(Node.Id);
        }

        public virtual void Deserialize(Serializer serializer)
        {
            Name = serializer.ReadString();
            PropertyName = serializer.ReadString();
            if (serializer.ReadBool())
                Description = serializer.ReadString();

            Color = Color.FromArgb(serializer.ReadInt());
            IsInput = serializer.ReadBool();
            if (serializer.ReadBool())
            {
                _hasConnection = true;
                serializer.PushPortConnection(Id, serializer.ReadGuid());
                serializer.PushPortConnection(Id, serializer.ReadGuid());
            }
            
            serializer.PushNodeRequest(Id, serializer.ReadGuid());
        }

        public virtual void EndDeserialize(Serializer serializer)
        {
            if (_hasConnection)
            {
                var from = serializer.NodesManager.GetPort(serializer.PopPortConnection(Id));
                var to = serializer.NodesManager.GetPort(serializer.PopPortConnection(Id));
                from.Connection = new Connection(from, to);
                to.Connection = from.Connection;   
            }

            Node = serializer.NodesManager.GetNode(serializer.PopNodeRequest(Id));
            var prop = Node.GetType().GetProperty(PropertyName);
            Color = prop?.GetCustomAttribute<ColorAttribute>()?.Color ?? Color;
        }
    }
}