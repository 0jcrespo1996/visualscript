﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Nodes;

namespace VisualScript.Core.Runtime.Ports
{
    [Entity("432E0827-C19B-4C3A-8498-D8D2BA441DD9")]
    public class ExecutePort : Port
    {
        public ExecutePort(Guid id) : base(id)
        {
        }

        public ExecutePort(INode node, Guid id, string name, string propertyName, bool isInput) : base(node, id, name, propertyName, isInput)
        {
        }

        public async Task ExecuteAsync(Context context)
        {
            Node.OnExecuting(this);
            IList<ExecutePort>? continues;
            try
            {
                if (Node.Manager.ExecutionDelay > 0)
                    await Task.Delay(Node.Manager.ExecutionDelay);
                continues = await Node.OnExecuteAsync(this, context);
            }
            catch (Exception e)
            {
                #if !DEBUG
                var ex = Node.OnErrored(this, e);
                if (ex != null)
                    throw ex;
                return;
                #endif
                
                #if DEBUG
                Node.OnErrored(this, e);
                throw;
#endif
            }
            Node.OnExecuted(this);
            if (continues == null) return;
            foreach (var cont in continues)
                if (cont.Connection?.To is ExecutePort {IsInput: true} tp)
                    await tp.ExecuteAsync(context);
        }

        public override bool CanConnectTo(IPort port)
        {
            return port is ExecutePort && base.CanConnectTo(port);
        }
    }
}