﻿using System;
using System.Threading.Tasks;

namespace VisualScript.Core.Runtime.Ports
{
    public interface IValuePort : IPort
    {
        Type ValueType { get; }
        object? ConstantValue { get; set; }
        Task<object?> GetValueAsync(Context context);
        event Action<IValuePort>? GettingValue;
        event Action<IValuePort>? GotValue;
        void OnGettingValue();
        void OnGotValue();
    }
}