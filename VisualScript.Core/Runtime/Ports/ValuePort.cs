﻿using System;
using System.Linq;
using System.Threading.Tasks;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Core.Serialization;

namespace VisualScript.Core.Runtime.Ports
{
    [Entity("5EDE72B2-6E0F-489D-B54F-F22E961A8360")]
    [Color("Green")]
    public class ValuePort <T> : Port, IValuePort
    {
        public Type ValueType { get; } = typeof(T);

        public ValuePort(Guid id) : base(id)
        {
        }

        public ValuePort(INode node, Guid id, string name, string propertyName, bool isInput) : base(node, id, name, propertyName, isInput)
        {
        }

        public T? ConstantValue
        {
            get => _constantValue;
            set
            {
                _hasconstant = true;
                _constantValue = value;
            }
        }

        private T? _constantValue;
        private bool _hasconstant;
        
        object? IValuePort.ConstantValue { get => ConstantValue; set => ConstantValue = (T) value!; }

        public event Action<IValuePort>? GettingValue;
        public event Action<IValuePort>? GotValue;

        public async Task<T?> GetValueAsync(Context context)
        {
            if (Node.Manager.ExecutionDelay > 0)
                await Task.Delay(Node.Manager.ExecutionDelay);
            OnGettingValue();
            if (context.TryGetResult<T>(Id, out var value))
            {
                goto finish;
            }
            if (_hasconstant && (IsInput && Connection == null || !IsInput && !Node.InputPorts.Any()))
            {
                value = ConstantValue;
            }
            else
            {
                var from = Connection?.From;
                if (from is not IValuePort valuePort) goto failed;
                if (from == this || !valuePort.ValueType.IsAssignableTo(ValueType)) goto failed;
                if (await valuePort.GetValueAsync(context) is T t)
                {
                    value = t;
                    goto finish;
                };
                
                goto failed;
            }
            
            finish:
            context.SetResult(this, value);
            OnGotValue();
            return value;
            failed:
            context.SetResult(this, null);
            OnGotValue();
            return default;
        }

        async Task<object?> IValuePort.GetValueAsync(Context context) => await GetValueAsync(context);

        public override void Serialize(Serializer serializer)
        {
            base.Serialize(serializer);
            serializer.Write(_hasconstant && ConstantValue != null);
            if (_hasconstant && ConstantValue != null)
            {
                serializer.Write(ConstantValue);
            }
        }

        public override void Deserialize(Serializer serializer)
        {
            base.Deserialize(serializer);
            if (serializer.ReadBool())
            {
                ConstantValue = serializer.ReadObject<T>();
            }
        }
        

        public override bool CanConnectTo(IPort port)
        {
            return port is IValuePort vp && (ValueType.IsAssignableTo(vp.ValueType) || vp.ValueType.IsAssignableTo(ValueType)) && base.CanConnectTo(port);
        }

        public void OnGettingValue()
        {
            GettingValue?.Invoke(this);
        }

        public void OnGotValue()
        {
            GotValue?.Invoke(this);
        }
    }
}