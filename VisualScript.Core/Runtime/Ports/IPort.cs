﻿using System.Drawing;
using VisualScript.Core.Runtime.Nodes;

namespace VisualScript.Core.Runtime.Ports
{
    public interface IPort : IEntity
    {
        string Name { get; set; }
        string PropertyName { get; }
        string? Description { get; set; }
        Color Color { get; set; }
        bool IsInput { get;}

        Connection? Connection { get; set; }

        INode Node { get; }

        bool CanConnectTo(IPort port);
        void ConnectTo(IPort port);
        void Disconnect();
    }
}