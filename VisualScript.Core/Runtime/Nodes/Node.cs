﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Threading.Tasks;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.Core.Serialization;

namespace VisualScript.Core.Runtime.Nodes
{
    public abstract class Node : INode
    {
        public Guid Id { get; private set; }

        public Vector2 Position { get; set; }
        public NodesManager Manager { get; }
        
        public string Name { get; }


        public IReadOnlyCollection<IPort> InputPorts => _inputPorts;

        public IReadOnlyCollection<IPort> OutputPorts => _outputPorts;
        
        protected readonly HashSet<IPort> _inputPorts = new();
        protected readonly HashSet<IPort> _outputPorts = new();

        public event Action<NodeEventArgs, ExecutePort>? Executing;
        public event Action<NodeEventArgs, ExecutePort>? Executed;
        public event Action<NodeEventArgs, ExecutePort, Exception>? Errored;

        public Node(Guid id, NodesManager manager)
        {
            Id = id;
            Manager = manager;
            Name = GetType().GetCustomAttribute<NodeAttribute>()!.Name;
        }


        public virtual Task<IList<ExecutePort>?> OnExecuteAsync(IPort executor, Context context)
        {
            return Task.FromResult<IList<ExecutePort>?>(null);
        }

        public void OnExecuting(ExecutePort port)
        {
            Executing?.Invoke(new NodeEventArgs(port, this), port);
        }

        public void AddPort(IPort port)
        {
            (port.IsInput ? _inputPorts : _outputPorts).Add(port);
        }

        public virtual void Serialize(Serializer serializer)
        {
            serializer.Write(Position.X);
            serializer.Write(Position.Y);
        }

        public virtual void Deserialize(Serializer serializer)
        {
            Position = new Vector2(serializer.ReadFloat(), serializer.ReadFloat());
        }

        public virtual void EndDeserialize(Serializer serializer)
        {
            
        }

        public virtual void OnInit()
        {
            
        }

        
        public T GetPort<T>(bool isInput, int index) where T : IPort
        {
            var target = isInput ? _inputPorts : _outputPorts;
            return (T) target.ElementAt(index);
        }


        public void AssignPorts()
        {
            var type = GetType();
            foreach (var port in _inputPorts)
            {
                var prop = type.GetProperty(port.PropertyName);
                if (prop != null)
                    prop.SetValue(this, port);
            }
            
            foreach (var port in _outputPorts)
            {
                var prop = type.GetProperty(port.PropertyName);
                if (prop != null)
                    prop.SetValue(this, port);
            }
        }

        public void OnExecuted(ExecutePort port)
        {
            Executed?.Invoke(new NodeEventArgs(port, this), port);
        }

        public Exception? OnErrored(ExecutePort port, Exception exception)
        {
            var eargs = new NodeEventArgs(port, this);
            Errored?.Invoke(eargs, port, exception);
            return eargs.Exception;
        }
    }
}