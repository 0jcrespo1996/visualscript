﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VisualScript.Core.Attributes;
using VisualScript.Core.Resources;
using VisualScript.Core.Runtime.Ports;
using VisualScript.Core.Serialization;

namespace VisualScript.Core.Runtime.Nodes
{
    [Node("EntryNodeName", typeof(strings))]
    [Entity("F0EB26C7-54FB-4241-982A-2463D8679930")]
    [Description("EntryNodeDescription", typeof(strings))]
    public class EntryNode : Node
    {
        [Port("")] public ExecutePort Execute { get; private set; } = null!;
        
        public EntryNode(Guid id, NodesManager manager) : base(id, manager)
        {
        }

        public async Task RunAsync(Context context)
        {
            var pair = Execute.Connection?.GetPair(Execute);
            if (pair is ExecutePort executePort) await executePort.ExecuteAsync(context);
        }
        
    }
}