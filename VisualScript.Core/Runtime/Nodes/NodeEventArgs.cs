﻿using System;
using VisualScript.Core.Runtime.Ports;

namespace VisualScript.Core.Runtime.Nodes
{
    public class NodeEventArgs : EventArgs
    {
        public IPort Port { get; }
        public INode Node { get; }
        public Exception? Exception { get; set; }

        public NodeEventArgs(IPort port, INode node)
        {
            Port = port;
            Node = node;
        }
    }
}