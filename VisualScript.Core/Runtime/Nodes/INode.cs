﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;
using VisualScript.Core.Runtime.Ports;

namespace VisualScript.Core.Runtime.Nodes
{
    public interface INode : IEntity
    {
        string Name { get; }
        Vector2 Position { get; set; }
        Task<IList<ExecutePort>?> OnExecuteAsync(IPort executor, Context context);
        NodesManager Manager { get; }
        void AddPort(IPort port);
        IReadOnlyCollection<IPort> InputPorts { get; }
        IReadOnlyCollection<IPort> OutputPorts { get; }
        void OnInit();
        event Action<NodeEventArgs, ExecutePort> Executing;
        event Action<NodeEventArgs, ExecutePort> Executed;
        event Action<NodeEventArgs, ExecutePort, Exception> Errored; 
        void OnExecuting(ExecutePort port);
        void OnExecuted(ExecutePort port);
        Exception? OnErrored(ExecutePort port, Exception exception);
        
        void AssignPorts();
    }
}