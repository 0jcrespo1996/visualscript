﻿using System.Linq;
using VisualScript.Core.Runtime.Ports;

namespace VisualScript.Core.Runtime
{
    public readonly struct Connection
    {
        public IPort From { get; }
        public IPort To { get; }

        public Connection(IPort from, IPort to)
        {
            if (from.IsInput)
            {
                var temp = from;
                from = to;
                to = temp;
            }
            From = from;
            To = to;
        }

        public IPort GetPair(IPort current)
        {
            return current.Id == From.Id ? To : From;
        }
    }
}