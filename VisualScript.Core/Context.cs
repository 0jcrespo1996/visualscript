﻿using System;
using System.Collections.Concurrent;
using Serilog;
using VisualScript.Core.Resources;

namespace VisualScript.Core
{
    public class Context
    {
        private readonly ConcurrentDictionary<Guid, object?> _variables = new();


        public void SetResult(Guid id, object? value)
        {
            _variables[id] = value;
        }

        public void SetResult(IEntity entity, object? value) => SetResult(entity.Id, value);

        public object? GetResult(Guid id)
        {
            if (_variables.TryGetValue(id, out var value)) return value;
            Log.Warning("A result for {@Id} was not set", id);
            value = null;

            return value;
        }

        public bool TryGetResult(Guid id, out object? res)
        {
            if (_variables.TryGetValue(id, out res)) return true;
            return false;
        }

        public object? GetResult(IEntity entity) => GetResult(entity.Id);


        public bool TryGetResult<T>(Guid id, out T value)
        {
            value = default!;
            if (TryGetResult(id, out var res))
            {
                if (res == default) return false;
                if (res is T t)
                {
                    value = t;
                    return true;
                }
                Log.Warning("A result for {@Id} of type {@TypeA} cannot be casted to {@TypeB}", id, res.GetType(), typeof(T));
            }

            return false;
        }
        public T? GetResult<T>(Guid id)
        {
            if (TryGetResult(id, out var res))
            {
                if (res == default) return default;
                if (res is T t) return t;
                Log.Warning("A result for {@Id} of type {@TypeA} cannot be casted to {@TypeB}", id, res.GetType(), typeof(T));
            }
            
            Log.Warning("A result for {@Id} was not set", id);
            return default;
        }

        public T GetResultStrict<T>(Guid id)
        {
            var res = GetResult(id);
            if (res is not T t)
                throw new InvalidCastException(string.Format(strings.ContextCannotGetResultAs, id, typeof(T)));

            return t;
        }
    }
}