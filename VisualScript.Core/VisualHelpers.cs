﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace VisualScript.Core
{
    public static class VisualHelpers
    {

        public static MethodInfo? GetConversionOperator(Type toType, Type fromType, out bool isInverted, bool isRepeat = false)
        {
            isInverted = false;
            var methods = toType.GetMethods(BindingFlags.Public | BindingFlags.Static);
            var ret = (from method in methods let isOp = method.Name.Equals("op_Implicit") || method.Name.Equals("op_Explicit") where isOp let args = method.GetParameters() let returnType = method.ReturnType where fromType.IsAssignableTo(args[0].ParameterType) where returnType.IsAssignableTo(toType) select method).FirstOrDefault();
            if (ret != null || isRepeat) return ret;
            isInverted = true;
            return GetConversionOperator(fromType, toType, out _, true);

        }
        
        public static bool TryCastTo(object? value, Type target, out object? result)
        {
            result = value;
            if (value == null) return false;
            var type = value.GetType();
            if (type.IsAssignableTo(target)) return true;
            
            if (value is IConvertible convertible)
            {
                try
                {
                    result = convertible.ToType(target, CultureInfo.InvariantCulture);
                    return true;
                }
                catch (Exception)
                {
                    // Ignored
                }
            }

            var parseMethod = target.GetMethod("Parse", BindingFlags.Public | BindingFlags.Static, null,
                CallingConventions.Any, new[] {type}, null);

            if (parseMethod != null)
            {
                try
                {
                    result = parseMethod.Invoke(null, new []{value});
                    return true;
                }
                catch (Exception)
                {
                    //
                }
            }
            
            var convOp = GetConversionOperator(target, type, out var isInverted);
            if (convOp == null || isInverted) return false;
            result = convOp.Invoke(null, new[] {value});
            return true;
        }
    }
}