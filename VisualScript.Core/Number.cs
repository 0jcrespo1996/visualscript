﻿using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace VisualScript.Core
{
    public readonly struct Number : IComparable, IConvertible, IFormattable, IComparable<double>, IEquatable<double> 
    {
        private readonly double _value;
        
        public Number(double value)
        {
            _value = value;
        }

        public object GetSmallestType()
        {
            return _value;
            /*
            if (_value % 1 != 0) return _value;
            var rounded = Math.Floor(_value);
            return rounded switch
            {
                <= sbyte.MaxValue and >= sbyte.MinValue => Convert.ChangeType(rounded, typeof(sbyte)),
                <= byte.MaxValue and >= byte.MinValue => Convert.ChangeType(rounded, typeof(byte)),
                <= short.MaxValue and >= short.MinValue => Convert.ChangeType(rounded, typeof(short)),
                <= ushort.MaxValue and >= ushort.MinValue => Convert.ChangeType(rounded, typeof(ushort)),
                <= int.MaxValue and >= int.MinValue => Convert.ChangeType(rounded, typeof(int)),
                <= uint.MaxValue and >= uint.MinValue => Convert.ChangeType(rounded, typeof(uint)),
                <= long.MaxValue and >= long.MinValue => Convert.ChangeType(rounded, typeof(long)),
                <= ulong.MaxValue and >= ulong.MinValue => Convert.ChangeType(rounded, typeof(ulong)),
                _ => _value
            };*/
        }
        

        public static implicit operator sbyte(Number number) => (sbyte) number._value;
        public static implicit operator byte(Number number) => (byte) number._value;
        public static implicit operator short(Number number) => (short) number._value;
        public static implicit operator ushort(Number number) => (ushort) number._value;
        public static implicit operator int(Number number) => (int) number._value;
        public static implicit operator uint(Number number) => (uint) number._value;
        public static implicit operator long(Number number) => (long) number._value;
        public static implicit operator ulong(Number number) => (ulong) number._value;
        public static implicit operator float(Number number) => (float) number._value;
        public static implicit operator double(Number number) => number._value;
        public static implicit operator decimal(Number number) => (decimal) number._value;

        public static implicit operator Number(sbyte value) => new(value);
        public static implicit operator Number(byte value) => new(value);
        public static implicit operator Number(short value) => new(value);
        public static implicit operator Number(ushort value) => new(value);
        public static implicit operator Number(int value) => new(value);
        public static implicit operator Number(uint value) => new(value);
        public static implicit operator Number(long value) => new(value);
        public static implicit operator Number(ulong value) => new(value);
        public static implicit operator Number(float value) => new(value);
        public static implicit operator Number(double value) => new(value);
        public static implicit operator Number(decimal value) => new((double) value);

        public override string ToString()
        {
            return _value.ToString(CultureInfo.InvariantCulture);
        }



        public string ToString(string? format, IFormatProvider? formatProvider)
        {
            return _value.ToString(format, formatProvider);
        }

        public int CompareTo(object? obj)
        {
            return _value.CompareTo(obj);
        }

        public int CompareTo(double other)
        {
            return _value.CompareTo(other);
        }

        public bool Equals(double other)
        {
            return _value.Equals(other);
        }

        public override bool Equals(object? obj)
        {
            return _value.Equals(obj);
        }

        public bool Equals(Number other)
        {
            return _value.Equals(other._value);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public static Number operator +(Number a, Number b) => a._value + b._value;
        public static Number operator -(Number a, Number b) => a._value - b._value;
        public static Number operator /(Number a, Number b) => a._value / b._value;
        public static Number operator *(Number a, Number b) => a._value * b._value;
        public static Number operator %(Number a, Number b) => a._value % b._value;
        public static bool operator <(Number a, Number b) => a._value < b._value;
        public static bool operator >(Number a, Number b) => a._value > b._value;
        public static bool operator <=(Number a, Number b) => a._value <= b._value;
        public static bool operator >=(Number a, Number b) => a._value  >=b._value;
        public static bool operator ==(Number a, Number b) => a._value.Equals(b._value);
        public static bool operator != (Number a, Number b) => !(a == b);

        public TypeCode GetTypeCode()
        {
            return _value.GetTypeCode();
        }

        public bool ToBoolean(IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToBoolean(provider);
        }

        public byte ToByte(IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToByte(provider);
        }

        public char ToChar(IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToChar(provider);
        }

        public DateTime ToDateTime(IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToDateTime(provider);
        }

        public decimal ToDecimal(IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToDecimal(provider);
        }

        public double ToDouble(IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToDouble(provider);
        }

        public short ToInt16(IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToInt16(provider);
        }

        public int ToInt32(IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToInt32(provider);
        }

        public long ToInt64(IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToInt64(provider);
        }

        public sbyte ToSByte(IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToSByte(provider);
        }

        public float ToSingle(IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToSingle(provider);
        }

        public string ToString(IFormatProvider? provider)
        {
            return _value.ToString(provider);
        }

        public object ToType(Type conversionType, IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToType(conversionType, provider);
        }

        public ushort ToUInt16(IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToUInt16(provider);
        }

        public uint ToUInt32(IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToUInt32(provider);
        }

        public ulong ToUInt64(IFormatProvider? provider)
        {
            return ((IConvertible) _value).ToUInt64(provider);
        }

        public static Number Parse(string input) => double.Parse(input);

        public static bool TryParse(string input, out Number number)
        {
            if (double.TryParse(input, NumberStyles.Any, CultureInfo.InvariantCulture, out var d))
            {
                number = d;
                return true;
            }

            number = default;
            return false;
        }
    }
}