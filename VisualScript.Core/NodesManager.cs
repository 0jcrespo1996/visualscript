﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using Serilog;
using VisualScript.Core.Attributes;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Core.Runtime.Ports;
using VisualScript.Core.Serialization;

namespace VisualScript.Core
{
    public class NodesManager
    {
        static NodesManager()
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .MinimumLevel.Information()
                .CreateLogger();
            
        }


        public int ExecutionDelay = 0;

        public event Action<string, object?>? VariableCreated;
        public event Action<string, object?>? VariableUpdated;
        
        public IDictionary<string, object?> GlobalsStorage { get; }
        
        public IReadOnlyCollection<INode> Nodes => _nodes.Values;
        public IReadOnlyCollection<IPort> Ports => _ports.Values;
        internal readonly Dictionary<Guid, INode> _nodes = new();
        internal readonly Dictionary<Guid, IPort> _ports = new();

        public NodesManager(IDictionary<string, object?>? globalsStorage = null)
        {
            GlobalsStorage = globalsStorage ?? new Dictionary<string, object?>();
        }


        public void SetVariable(string name, object? value)
        {
            var contains = GlobalsStorage.ContainsKey(name);
            GlobalsStorage[name] = value;
            (contains ? VariableUpdated : VariableCreated)?.Invoke(name, value);
        }

        public object? GetVariable(string name)
        {
            GlobalsStorage.TryGetValue(name, out var value);
            return value;
        }
        
        public Guid? EntryNode { get; set; }


        public EntryNode? GetEntryNode()
        {
            if (EntryNode == null) return null;
            return _nodes.TryGetValue(EntryNode.Value, out var node) ? node as EntryNode : null;
        }

        public INode GetNode(Guid id)
        {
            return _nodes[id];
        }

        public IPort GetPort(Guid id)
        {
            return _ports[id];
        }


        public void RemoveNode(Guid id)
        {
            if (_nodes.TryGetValue(id, out var node))
                RemoveNode(node);
        }

        public void RemoveNode(INode node)
        {
          
            _nodes.Remove(node.Id);
            foreach (var port in node.InputPorts)
            {
                port.Disconnect();
                _ports.Remove(port.Id);
            }

            foreach (var port in node.OutputPorts)
            {
                port.Disconnect();
                _ports.Remove(port.Id);
            }

            if (node.Id == EntryNode)
                EntryNode = null;
            
        }

        public INode CreateNode(Type type, Guid id, bool createPorts = true)
        {
            var instance = (INode)Activator.CreateInstance(type, id, this)!;
            if (instance is EntryNode entryNode) EntryNode = entryNode.Id;
            _nodes.Add(id, instance);
            if (createPorts) CreatePorts(instance);
            instance.OnInit();
            return instance;
        }

        public void AddPort(IPort port)
        {
            _ports.TryAdd(port.Id, port);
        }

        public void RemovePort(IPort port)
        {
            _ports.Remove(port.Id);
        }
        private void CreatePorts(INode node)
        {
            var type = node.GetType();
            var portsProps = type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p =>
                p.IsDefined(typeof(PortAttribute)) && p.PropertyType.IsAssignableTo(typeof(Port)));
            
            foreach (var prop in portsProps)
            {
                var portAttr = prop.GetCustomAttribute<PortAttribute>()!;
                var descAttr = prop.GetCustomAttribute<DescriptionAttribute>();
                var guid = Guid.NewGuid();
                var instance = (IPort) Activator.CreateInstance(prop.PropertyType, node, guid,
                    portAttr.Name ?? prop.Name, prop.Name, portAttr.IsInput)!;

                if (descAttr != null)
                {
                    var propName = (prop.PropertyType.IsGenericType
                        ? prop.PropertyType.GetGenericArguments()[0].Name
                        : prop.PropertyType.Name);
                    
                    instance.Description = $"{descAttr.Description}\n({propName})";
                }
                

                var setter = prop.GetSetMethod(true);
                if (setter == null)
                    throw new InvalidOperationException($"Property {prop} of type {type} does not have a setter");
                setter!.Invoke(node, new object?[] {instance});
                _ports.Add(instance.Id, instance);
                node.AddPort(instance);
            }
        }

        public T CreateNode<T>() where T : INode => CreateNode<T>(Guid.NewGuid());
        public T CreateNode<T>(Guid id) where T : INode
        {
            return (T) CreateNode(typeof(T), id);
        }

        public byte[] Serialize()
        {
            var ser = new Serializer(this);
            var bytes = ser.Serialize();
            using var memStream = new MemoryStream();
            using var gzip = new GZipStream(memStream, CompressionLevel.Optimal);
            gzip.Write(bytes);
            gzip.Flush();
            return memStream.ToArray();
        }

        public void Deserialize(byte[] data)
        {
            Deserialize(data, this);
        }

        // ReSharper disable once MethodOverloadWithOptionalParameter
        public static NodesManager Deserialize(byte[] data, NodesManager? manager = null)
        {
            manager ??= new NodesManager();
            using var memStream = new MemoryStream(data);
            using var gzipStream = new GZipStream(memStream, CompressionMode.Decompress);
            using var decompStream = new MemoryStream();
            gzipStream.CopyTo(decompStream);
            decompStream.Seek(0, SeekOrigin.Begin);
            var serializer = new Serializer(manager, decompStream);
            serializer.Deserialize();
            return manager;
        }

        public void Clear()
        {
            var nodes = _nodes.Values.ToArray();
            foreach (var node in nodes)
                RemoveNode(node);
            EntryNode = null;
        }
    }
}