﻿namespace VisualScript.Core.Serialization
{
    public static class ReservedIds
    {
        public const int List = -200;
        public const int Port = -201;
        public const int Node = -202;
    }
}