﻿namespace VisualScript.Core.Serialization
{
    public interface ISerializable
    {
        
        void Serialize(Serializer serializer);
        
        
        void Deserialize(Serializer serializer);
        
        /// <summary>
        /// Called when the data stream finishes deserializing
        /// </summary>
        /// <param name="serializer"></param>
        void EndDeserialize(Serializer serializer);
    }
}