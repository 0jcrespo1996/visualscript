﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using BinaryFormatter;
using VisualScript.Core.Runtime;
using VisualScript.Core.Runtime.Nodes;
using VisualScript.Core.Runtime.Ports;

namespace VisualScript.Core.Serialization
{
    public class Serializer
    {
        public Stream Stream { get; }
        public NodesManager NodesManager { get; }

        public BinaryConverter BinaryConverter { get; } = new();

        private readonly Dictionary<Guid, Queue<Guid>> _requestedPorts = new();
        private readonly Dictionary<Guid, Queue<Guid>> _requestedNodes = new();

        public Serializer(NodesManager nodesManager)
        {
            Stream = new MemoryStream();
            NodesManager = nodesManager;
        }

        public Serializer(NodesManager nodesManager, Stream stream)
        {
            Stream = stream;
            NodesManager = nodesManager;
        }

        public void PushPortConnection(Guid port, Guid target)
        {
            if (!_requestedPorts.TryGetValue(port, out var queue))
            {
                queue = new Queue<Guid>();
                _requestedPorts.Add(port, queue);
            }
            
            queue.Enqueue(target);
        }

        public Guid PopPortConnection(Guid port)
        {
            return _requestedPorts[port].Dequeue();
        }
        
        public void PushNodeRequest(Guid port, Guid nodeId)
        {
            if (!_requestedNodes.TryGetValue(port, out var queue))
            {
                queue = new Queue<Guid>();
                _requestedNodes.Add(port, queue);
            }
            
            queue.Enqueue(nodeId);
        }

        public Guid PopNodeRequest(Guid port)
        {
            return _requestedNodes[port].Dequeue();
        }

        public byte[] Serialize()
        {
            var memStream = new MemoryStream();
            var nodes = NodesManager.Nodes;
            Write(nodes.Count);
            Write(NodesManager.EntryNode != null);
            if (NodesManager.EntryNode != null)
                Write(NodesManager.EntryNode.Value);
            foreach (var node in nodes)
            {
                var type = node.GetType();
                Write(type.AssemblyQualifiedName!);
                Write(node.Id);
                node.Serialize(this);
                
                Write(node.InputPorts.Count);
                foreach (var port in node.InputPorts)
                {
                    Write(port.GetType().AssemblyQualifiedName!);
                    port.Serialize(this);
                }
                
                Write(node.OutputPorts.Count);
                foreach (var port in node.OutputPorts)
                {
                    Write(port.GetType().AssemblyQualifiedName!);
                    port.Serialize(this);
                }
            }

            Stream.Position = 0;
            Stream.CopyTo(memStream);
            return memStream.ToArray();
        }
        
        public void Deserialize()
        {
            if (Stream.CanSeek) Stream.Seek(0, SeekOrigin.Begin);
            var count = ReadInt();
            if (ReadBool())
                NodesManager.EntryNode = ReadGuid();
            var deserializedNodes = new List<INode>();
            var deserializedPorts = new List<IPort>();

            for (var i = 0; i < count; i++)
            {
                var node = DeserializeNode();
                deserializedNodes.Add(node);
                var inputsCount = ReadInt();
                for (var j = 0; j < inputsCount; j++)
                {
                    var port = DeserializePort(node);
                    deserializedPorts.Add(port);
                }

                var outputsCount = ReadInt();
                for (var j = 0; j < outputsCount; j++)
                {
                    var port = DeserializePort(node);
                    deserializedPorts.Add(port);
                }
            }

            foreach (var node in deserializedNodes)
            {
                node.EndDeserialize(this);
                node.AssignPorts();
            }

            foreach (var port in deserializedPorts)
                port.EndDeserialize(this);
        }

        private IPort DeserializePort(INode node)
        {
            var typeName = ReadString();
            var type = Type.GetType(typeName)!;

            var id = ReadGuid();
            var instance = (IPort) Activator.CreateInstance(type, id)!;
            instance.Deserialize(this);
            node.AddPort(instance);
            NodesManager._ports.Add(instance.Id, instance);
            return instance;
        }
        
        private INode DeserializeNode()
        {
            var typeName = ReadString();
            var type = Type.GetType(typeName)!;
            var id = ReadGuid();
            var node = (INode) Activator.CreateInstance(type, id, NodesManager)!;
            NodesManager._nodes.Add(node.Id, node);
            node.Deserialize(this);
            return node;
        }
        

        public void Restart()
        {
            Stream.Position = 0;
            _requestedNodes.Clear();
            _requestedPorts.Clear();
        }

        #region Writing/Reading
        public void Write(byte value) => Stream.WriteByte(value);
        public void Write(byte[] arr) => Stream.Write(arr, 0, arr.Length);
        public void Write(sbyte value) => Stream.Write(BitConverter.GetBytes(value));
        public void Write(short value) => Stream.Write(BitConverter.GetBytes(value));
        public void Write(ushort value) => Stream.Write(BitConverter.GetBytes(value));
        public void Write(int value) => Stream.Write(BitConverter.GetBytes(value));
        public void Write(uint value) => Stream.Write(BitConverter.GetBytes(value));
        public void Write(long value) => Stream.Write(BitConverter.GetBytes(value));
        public void Write(ulong value) => Stream.Write(BitConverter.GetBytes(value));
        public void Write(float value) => Stream.Write(BitConverter.GetBytes(value));
        public void Write(double value) => Stream.Write(BitConverter.GetBytes(value));
        public void Write(bool value) => Stream.Write(BitConverter.GetBytes(value));
        public void Write(Guid guid) => Stream.Write(guid.ToByteArray());

        public void Write(object obj)
        {
            if (obj is Number number)
            {
                obj = number.GetSmallestType();
                var type = obj switch
                {
                    sbyte => 0,
                    byte => 1,
                    short => 2,
                    ushort => 3,
                    int => 4,
                    uint => 5,
                    long => 6,
                    ulong => 7,
                    _ => 8
                };
                Write((byte)type);
            }
            var bytes = BinaryConverter.Serialize(obj);
            Write(bytes.Length);
            Write(bytes);
        }

        public void Write(string str) => WriteString(str);
        
        public void WriteString(string str)
        {
            var bytes = Encoding.UTF8.GetBytes(str);
            Write(bytes.Length);
            Write(bytes);
        }


        private byte[] ReadBigEndian(int count)
        {
            var buffer = new byte[count];
            Stream.Read(buffer);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buffer);

            return buffer;
        }

        public short ReadShort()
        {
            return BitConverter.ToInt16(ReadBigEndian(sizeof(short)));
        }
        
        public ushort ReadUShort()
        {
            return BitConverter.ToUInt16(ReadBigEndian(sizeof(ushort)));
        }
        
        public int ReadInt()
        {
            return BitConverter.ToInt32(ReadBigEndian(sizeof(int)));
        }
        
        public uint ReadUInt()
        {
            return BitConverter.ToUInt32(ReadBigEndian(sizeof(uint)));
        }
        
        public long ReadLong()
        {
            return BitConverter.ToInt64(ReadBigEndian(sizeof(long)));
        }
        
        public ulong ReadULong()
        {
            return BitConverter.ToUInt64(ReadBigEndian(sizeof(ulong)));
        }
        
        public float ReadFloat()
        {
            return BitConverter.ToSingle(ReadBigEndian(sizeof(float)));
        }
        
        public double ReadDouble()
        {
            return BitConverter.ToDouble(ReadBigEndian(sizeof(double)));
        }
        
        public bool ReadBool()
        {
            return BitConverter.ToBoolean(ReadBigEndian(sizeof(bool)));
        }
        
        public Guid ReadGuid()
        {
            Span<byte> buffer = stackalloc byte[16];
            Stream.Read(buffer);
            return new Guid(buffer);
        }

        public string ReadString()
        {
            var length = ReadInt();
            var buffer = new byte[length];
            Stream.Read(buffer);
            return Encoding.UTF8.GetString(buffer);
        }

        public int ReadId() => ReadInt();

        public T ReadObject<T>()
        {
            byte type = 0;
            if (typeof(T) == typeof(Number)) type = (byte) Stream.ReadByte(); 
            var count = ReadInt();
            Span<byte> buffer = stackalloc byte[count];
            Stream.Read(buffer);
            var arrBuffer = buffer.ToArray();
            if (typeof(T) != typeof(Number)) return BinaryConverter.Deserialize<T>(arrBuffer);
            
            var obj = type switch
            {
                0 => BinaryConverter.Deserialize<sbyte>(arrBuffer),
                1 => BinaryConverter.Deserialize<byte>(arrBuffer),
                2 => BinaryConverter.Deserialize<short>(arrBuffer),
                3 => BinaryConverter.Deserialize<ushort>(arrBuffer),
                4 => BinaryConverter.Deserialize<int>(arrBuffer),
                5 => BinaryConverter.Deserialize<uint>(arrBuffer),
                6 => BinaryConverter.Deserialize<long>(arrBuffer),
                7 => BinaryConverter.Deserialize<ulong>(arrBuffer),
                _ => BinaryConverter.Deserialize<double>(arrBuffer),
            };

            return (T)(object) new Number(obj);
        }

        #endregion
    }
}