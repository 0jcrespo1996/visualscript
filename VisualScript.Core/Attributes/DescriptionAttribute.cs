﻿using System;
using System.Reflection;

namespace VisualScript.Core.Attributes
{
    public class DescriptionAttribute : VisualAttribute
    {
        public string Description { get; }

        public DescriptionAttribute(string description, Type? resourceType = null)
        {
            if (resourceType != null)
            {
                var prop = resourceType.GetProperty(description,
                    BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
                if (prop != null)
                {
                    if (prop.GetValue(null) is string str)
                    {
                        Description = str;
                        return;
                    }
                    
                }
            }
            Description = description;
        }
    }
}