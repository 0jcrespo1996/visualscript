﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace VisualScript.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class CategoryAttribute : VisualAttribute
    {
        public string[] Path { get; }

        public const string DefaultPath = "Misc";

        public CategoryAttribute(params string[] path)
        {
            if (path.Length == 0)
                path = new[] {DefaultPath};
            Path = path;
        }

        public CategoryAttribute(Type resourceType, params string[] path)
        {
            Path = new string[path.Length];
            for (int i = 0; i < path.Length; i++)
            {
                var prop = resourceType.GetProperty(path[i],
                        BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
                if (prop != null)
                {
                    if (prop.GetValue(null) is string str)
                    {
                        Path[i] = str;
                    }
                    else
                    {
                        Path[i] = path[i];
                    }
                
                }
            }
        }
    }
}