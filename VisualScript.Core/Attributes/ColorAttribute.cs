﻿using System;
using System.Drawing;
using VisualScript.Core.Runtime.Ports;

namespace VisualScript.Core.Attributes
{
    public class ColorAttribute : VisualAttribute
    {
        public Color Color { get; }

        public ColorAttribute(string name)
        {
            Color = Color.FromKnownColor(Enum.Parse<KnownColor>(name));
        }

        public ColorAttribute(byte r, byte g, byte b, byte a = 255)
        {
            Color = Color.FromArgb(a, r, g, b);
        }


        public static Color ForValuePort(IValuePort valuePort)
        {
            return valuePort.ConstantValue switch
            {
                string => Color.Green,
                Number => Color.Cyan,
                bool => Color.Blue,
                Enum => Color.Yellow,
                _ => Color.Brown
            };
        }
    }
}