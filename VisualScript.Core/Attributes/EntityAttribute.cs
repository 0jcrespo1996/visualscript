﻿using System;

namespace VisualScript.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface, Inherited = false)]
    public class EntityAttribute : VisualAttribute
    {
        public Guid Guid { get; }

        public EntityAttribute(string guid)
        {
            Guid = Guid.Parse(guid);
        }
    }
}