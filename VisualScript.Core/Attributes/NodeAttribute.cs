﻿using System;
using System.Reflection;

namespace VisualScript.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = false)]
    public class NodeAttribute : VisualAttribute
    {
        public string Name { get; }
        public NodeAttribute(string name = "", Type? resourceType = null!)
        {
            if (resourceType != null)
            {
                var prop = resourceType.GetProperty(name,
                    BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
                if (prop != null)
                {
                    if (prop.GetValue(null) is string str)
                    {
                        Name = str;
                        return;
                    }
                    
                }
            }
            Name = name;
        }
        
    }
}