﻿using System;
using System.Reflection;
using VisualScript.Core.Resources;

namespace VisualScript.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class PortAttribute : VisualAttribute
    {
        public string? Name { get; }
        public bool IsInput { get; }
        
        public int Position { get; }

        public PortAttribute( string? name = null, bool isInput = false, Type? resourceType = null!)
        {
            IsInput = isInput;
            if (resourceType != null && name != null)
            {
                var prop = resourceType.GetProperty(name,
                    BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
                if (prop != null)
                {
                    if (prop.GetValue(null) is string str)
                    {
                        Name = str;
                        return;
                    }
                    
                }
            }
            Name = name;
        }
    }
}