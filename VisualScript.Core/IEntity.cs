﻿using System;
using VisualScript.Core.Serialization;

namespace VisualScript.Core
{
    public interface IEntity : ISerializable
    {
        Guid Id { get; }
        
        
    }
}